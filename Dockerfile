ARG CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX=docker.io
FROM --platform=$BUILDPLATFORM ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/golang:1.16.3 AS base

FROM scratch
COPY --from=base /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
ARG CI_PROJECT_NAME
COPY ${CI_PROJECT_NAME} /terraform-module-registry-for-gitlab
ENTRYPOINT ["/terraform-module-registry-for-gitlab"]
