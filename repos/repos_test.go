package repos_test

import (
	"context"
	"sort"
	"testing"

	"github.com/hashicorp/go-version"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/repos"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/testhelpers"
)

func TestModuleUsageHCL(t *testing.T) {
	t.Parallel()
	_, mockLister, _, server, _, err := testhelpers.Setup(t, testhelpers.MockCase{
		FixturesDir: "fixtures",
		ModuleRepos: []*testhelpers.MockModuleRepo{
			{
				ProjectID:         1,
				PathWithNamespace: "mock-modules/terraform-aws-public-module",
				Versions: testhelpers.NewMockVersions(t, []*testhelpers.MockModuleVersionInput{
					{
						TagName:     "v1.0.0",
						FS:          testhelpers.Fixtures,
						FixturesDir: "fixtures",
					},
				}),
			},
			{
				ProjectID:         2,
				PathWithNamespace: "mock-modules/terraform-azurerm-public-module",
				Versions: testhelpers.NewMockVersions(t, []*testhelpers.MockModuleVersionInput{
					{
						TagName:     "v1.0.0",
						FS:          testhelpers.InvalidModuleFixtures,
						FixturesDir: "invalid_module_fixtures",
					},
				}),
			},
		},
	})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	for _, testCase := range []struct {
		namespace, name, provider, version, tagName string
		expectError                                 bool
	}{
		{
			namespace: "mock-modules",
			name:      "public-module",
			provider:  "aws",
			version:   "1.0.0",
			tagName:   "v1.0.0",
		},
		{
			namespace:   "mock-modules",
			name:        "public-module",
			provider:    "azurerm",
			version:     "1.0.0",
			tagName:     "v1.0.0",
			expectError: true,
		},
		{
			namespace:   "mock-modules",
			name:        "public-module",
			provider:    "azurerm",
			version:     "1.0.0",
			tagName:     "not-even-close",
			expectError: true,
		},
	} {
		moduleRepo := mockLister.GetModuleRepo(testCase.namespace, testCase.name, testCase.provider)
		moduleVersions := mockLister.GetModuleVersions(context.Background(), moduleRepo)
		v := moduleVersions[testCase.version]
		err := v.EnsureModuleInfo(context.Background(), mockLister, testCase.tagName)
		result := v.ModuleUsageHCL(server.URL)
		require.NotEmpty(t, result)
		if testCase.expectError {
			require.Error(t, err)
			require.Contains(t, result, "No module read for tag")
		} else {
			require.NoError(t, err)
		}
	}
}

func TestEnsureModuleInfoInvalidFixture(t *testing.T) {
	t.Parallel()
	_, l, _, server, _, err := testhelpers.Setup(t, testhelpers.MockCase{
		FixturesDir: "invalid_module_fixtures",
		ModuleRepos: []*testhelpers.MockModuleRepo{
			{
				ProjectID:         1,
				PathWithNamespace: "mock-modules/terraform-aws-public-module",
				Versions: testhelpers.NewMockVersions(t, []*testhelpers.MockModuleVersionInput{
					{
						TagName:     "v1.0.0",
						FS:          testhelpers.InvalidModuleFixtures,
						FixturesDir: "invalid_module_fixtures",
					},
				}),
			},
		},
	})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	moduleRepo := l.GetModuleRepo("mock-modules", "public-module", "aws")
	moduleVersions := l.GetModuleVersions(context.Background(), moduleRepo)
	v := moduleVersions["1.0.0"]
	require.Error(t, v.EnsureModuleInfo(context.Background(), l, "v1.0.0"))
}

func TestNewListerInitialized(t *testing.T) {
	t.Parallel()
	_, _, _, server, client, err := testhelpers.Setup(t, testhelpers.MockCase{})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	l := repos.NewLister(client, logrus.NewEntry(logrus.StandardLogger()), "mock-modules", false)
	require.False(t, l.Initialized(context.Background()))
}

func TestNewListerInitializeNoProjectsNoError(t *testing.T) {
	t.Parallel()
	_, l, _, server, _, err := testhelpers.Setup(t, testhelpers.MockCase{})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	require.NoError(t, l.Initialize(context.Background()))
}

func TestNewListerInitializeSingleProject(t *testing.T) {
	t.Parallel()
	_, l, _, server, _, err := testhelpers.SetupSingleRepo(t)
	t.Cleanup(server.Close)
	require.NoError(t, err)

	require.NoError(t, l.Initialize(context.Background()))
}

func TestNewListerInitializeProjectOutsideGroup(t *testing.T) {
	t.Parallel()
	_, _, _, server, client, err := testhelpers.Setup(t, testhelpers.MockCase{})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	l := repos.NewLister(client, logrus.NewEntry(logrus.StandardLogger()), "blah-blah-blah", false)
	require.Error(t, l.Initialize(context.Background()))
}

func TestNewListerInitializeNonSemverTags(t *testing.T) {
	t.Parallel()
	_, l, _, server, _, err := testhelpers.Setup(t, testhelpers.MockCase{
		FixturesDir:  "fixtures",
		Users:        testhelpers.DefaultUsers,
		GroupMembers: testhelpers.DefaultMembers,
		ModuleRepos: []*testhelpers.MockModuleRepo{
			{
				ProjectID:         1,
				PathWithNamespace: "mock-modules/terraform-module-bogus",
				Versions: testhelpers.MockVersions{
					VersionsByTagName: map[string]*testhelpers.MockModuleVersion{
						"foo": {},
					},
				},
			},
		},
	})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	require.NoError(t, l.Initialize(context.Background()))
}

func TestGetModuleRepo(t *testing.T) {
	t.Parallel()
	_, l, _, server, _, err := testhelpers.SetupSingleRepo(t)
	t.Cleanup(server.Close)
	require.NoError(t, err)

	moduleRepo, err := l.UpsertModuleRepo(context.Background(), "mock-modules/terraform-aws-public-module")
	require.NoError(t, err)
	require.NotNil(t, moduleRepo)
	require.NotNil(t, l.GetModuleRepo("mock-modules", "public-module", "aws"))
	require.Nil(t, l.GetModuleRepo("foo", "bar", "baz"))
}

func TestGetModuleReposModuleOutsideSubgroup(t *testing.T) {
	t.Parallel()
	server, client, _ := testhelpers.SetupServerClientMux(t)
	t.Cleanup(server.Close)

	l := repos.NewLister(client, logrus.NewEntry(logrus.StandardLogger()), "mock-modules", true)

	_, err := l.UpsertModuleRepo(context.Background(), "some-other-modules/terraform-module-bogus")
	require.Error(t, err)
}

func TestGetModuleRepos(t *testing.T) {
	t.Parallel()
	server, client, mux := testhelpers.SetupServerClientMux(t)
	t.Cleanup(server.Close)
	project := gitlab.Project{
		ID:                1,
		PathWithNamespace: "mock-modules/terraform-aws-public-module",
		Namespace: &gitlab.ProjectNamespace{
			Path: "mock-modules",
		},
	}
	mockModuleRepo := testhelpers.MockModuleRepo{
		ProjectID:         1,
		PathWithNamespace: "mock-modules/terraform-aws-public-module",
	}
	mockCase := testhelpers.MockCase{
		Users:        testhelpers.DefaultUsers,
		GroupMembers: testhelpers.DefaultMembers,
		ModuleRepos:  []*testhelpers.MockModuleRepo{&mockModuleRepo},
	}
	testhelpers.HandleGroup(t, mockCase, mux, []*gitlab.Project{&project})
	testhelpers.HandleProject(t, &project, mux)

	l := repos.NewLister(client, logrus.NewEntry(logrus.StandardLogger()), "mock-modules", false)

	require.Empty(t, l.GetModuleRepos(context.Background()))
	moduleRepo, err := l.UpsertModuleRepo(context.Background(), "mock-modules/terraform-aws-public-module")
	require.NoError(t, err)
	require.NotNil(t, moduleRepo)

	moduleRepos := l.GetModuleRepos(context.Background())
	require.NotEmpty(t, moduleRepos)
	require.Equal(t, moduleRepos[moduleRepo.Project.PathWithNamespace].String(), moduleRepo.String())

	_, err = l.UpsertModuleRepo(context.Background(), "mock-modules/terraform-module-bogus")
	require.Error(t, err)

	_, err = l.UpsertModuleRepo(context.Background(), "mock-modules/not-even-a-terraform-module")
	require.Error(t, err)

	_, err = l.UpsertModuleRepo(context.Background(), "mock-modules/sub-group/terraform-module-bogus")
	require.Error(t, err)
}

func TestGetModuleVersions(t *testing.T) {
	t.Parallel()
	_, _, _, server, client, err := testhelpers.SetupSingleRepo(t)
	t.Cleanup(server.Close)
	require.NoError(t, err)

	l := repos.NewLister(client, logrus.NewEntry(logrus.StandardLogger()), "mock-modules", false)

	moduleRepo, err := l.UpsertModuleRepo(context.Background(), "mock-modules/terraform-aws-public-module")
	require.NoError(t, err)
	require.NotNil(t, moduleRepo)

	require.Nil(t, l.GetModuleVersions(context.Background(), moduleRepo))

	moduleVersion, err := l.UpsertModuleVersion(context.Background(), moduleRepo, "v2.3.0")
	require.NoError(t, err)
	require.NotNil(t, moduleVersion)

	moduleVersion, err = l.UpsertModuleVersion(context.Background(), moduleRepo, "v3.0.0-beta.1")
	require.NoError(t, err)
	require.NotNil(t, moduleVersion)

	_, err = l.UpsertModuleVersion(context.Background(), moduleRepo, "bogus-version")
	require.Error(t, err)

	versions := l.GetModuleVersions(context.Background(), moduleRepo)
	require.NotNil(t, versions)
	expectedVersions := []string{"2.3.0", "3.0.0-beta.1"}
	versionsSeen := []string{}
	for _, v := range versions {
		versionsSeen = append(versionsSeen, v.Version.String())
	}
	sort.Strings(versionsSeen)
	sort.Strings(expectedVersions)
	require.Equal(t, expectedVersions, versionsSeen)
}

func TestConsumeDownloadToken(t *testing.T) {
	t.Parallel()
	_, _, _, server, client, err := testhelpers.SetupSingleRepo(t)
	t.Cleanup(server.Close)
	require.NoError(t, err)

	l := repos.NewLister(client, logrus.NewEntry(logrus.StandardLogger()), "mock-modules", false)

	moduleRepo, err := l.UpsertModuleRepo(context.Background(), "mock-modules/terraform-aws-public-module")
	require.NoError(t, err)
	require.NotNil(t, moduleRepo)

	require.Nil(t, l.GetModuleVersions(context.Background(), moduleRepo))

	moduleVersion, err := l.UpsertModuleVersion(context.Background(), moduleRepo, "v2.3.0")
	require.NoError(t, err)
	require.NotNil(t, moduleVersion)

	// Test generating a token
	token, err := l.GenerateDownloadToken(context.Background(), moduleRepo.Namespace, moduleRepo.Name, moduleRepo.Provider, moduleVersion.Version.String())
	require.NoError(t, err)

	// Test consuming the token
	retrievedModuleVersion, err := l.ConsumeDownloadToken(context.Background(), token)
	require.NoError(t, err)

	require.Equal(t, *moduleVersion, *retrievedModuleVersion)

	// Test consuming the token a second time
	_, err = l.ConsumeDownloadToken(context.Background(), token)
	require.Error(t, err)

	// Test generating a token for a non-existing project
	_, err = l.GenerateDownloadToken(context.Background(), "foo", "bar", "baz", "1.2.3")
	require.Error(t, err)

	// Test generating a token for a non-existing version
	_, err = l.GenerateDownloadToken(context.Background(), moduleRepo.Namespace, moduleRepo.Name, moduleRepo.Provider, "9000.1.2")
	require.Error(t, err)
}

func TestModuleVersionListSort(t *testing.T) {
	t.Parallel()
	list := repos.ModuleVersionList{
		&repos.ModuleVersion{
			Version: version.Must(version.NewSemver("1.0.0")),
		},
		&repos.ModuleVersion{
			Version: version.Must(version.NewSemver("2.1.0")),
		},
		&repos.ModuleVersion{
			Version: version.Must(version.NewSemver("1.1.0")),
		},
	}
	require.False(t, sort.IsSorted(list))
	sort.Sort(list)
	require.True(t, sort.IsSorted(list))

	expected := repos.ModuleVersionList{
		&repos.ModuleVersion{
			Version: version.Must(version.NewSemver("1.0.0")),
		},
		&repos.ModuleVersion{
			Version: version.Must(version.NewSemver("1.1.0")),
		},
		&repos.ModuleVersion{
			Version: version.Must(version.NewSemver("2.1.0")),
		},
	}
	require.Equal(t, expected, list)
}

func TestModuleRepoList(t *testing.T) {
	t.Parallel()
	list := repos.ModuleRepoList{
		&repos.ModuleRepo{
			Namespace: "b",
			Name:      "a",
			Provider:  "a",
		},
		&repos.ModuleRepo{
			Namespace: "c",
			Name:      "a",
			Provider:  "a",
		},
		&repos.ModuleRepo{
			Namespace: "b",
			Name:      "b",
			Provider:  "a",
		},
	}
	require.False(t, sort.IsSorted(list))
	sort.Sort(list)
	require.True(t, sort.IsSorted(list))

	expected := repos.ModuleRepoList{
		&repos.ModuleRepo{
			Namespace: "b",
			Name:      "a",
			Provider:  "a",
		},
		&repos.ModuleRepo{
			Namespace: "b",
			Name:      "b",
			Provider:  "a",
		},
		&repos.ModuleRepo{
			Namespace: "c",
			Name:      "a",
			Provider:  "a",
		},
	}
	require.Equal(t, expected, list)
}
