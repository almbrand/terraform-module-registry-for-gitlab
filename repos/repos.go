package repos

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/fs"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
	"sync"
	"time"

	version "github.com/hashicorp/go-version"
	"github.com/hashicorp/hcl/v2/hclsyntax"
	"github.com/hashicorp/hcl/v2/hclwrite"
	"github.com/hashicorp/terraform/configs"
	"github.com/sirupsen/logrus"
	"github.com/spf13/afero"
	"github.com/xanzy/go-gitlab"
	"github.com/zclconf/go-cty/cty"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/helpers"
)

// ModuleProjectName matches Terraform module project names and contains a group for the provider segment
var ModuleProjectName = regexp.MustCompile(`terraform-(?P<provider>[^-]+)-.+`)

// ModuleRepo represents a repository that contains a module
type ModuleRepo struct {
	Namespace     string           `json:"namespace,omitempty"`
	Name          string           `json:"name,omitempty"`
	Provider      string           `json:"provider,omitempty"`
	URL           string           `json:"url,omitempty"`
	LatestVersion *version.Version `json:"latest_version,omitempty"`

	Project *gitlab.Project `json:"project,omitempty"`
}

func (r ModuleRepo) String() string {
	return fmt.Sprintf("%s/%s/%s", r.Namespace, r.Name, r.Provider)
}

// ModuleRepoList is a slice of module repositories that can be sorted by their
// module address, i.e. namespace, name, provider, in that order.
type ModuleRepoList []*ModuleRepo

func (a ModuleRepoList) Len() int      { return len(a) }
func (a ModuleRepoList) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ModuleRepoList) Less(i, j int) bool {
	ai := fmt.Sprintf("%s/%s/%s", a[i].Namespace, a[i].Name, a[i].Provider)
	aj := fmt.Sprintf("%s/%s/%s", a[j].Namespace, a[j].Name, a[j].Provider)
	return ai < aj
}

// ModuleVersion represents a version of a module by virtue of a tag name and
// semantic version, and contains a Terraform config module once parsed.
type ModuleVersion struct {
	ModuleRepo *ModuleRepo      `json:"module_repo,omitempty"`
	Version    *version.Version `json:"version,omitempty"`
	TagName    string           `json:"tag_name,omitempty"`
	WebURL     string           `json:"web_url,omitempty"`
	Readme     string           `json:"readme,omitempty"`
	Module     *configs.Module
}

// ModuleVersionList is a slice of module versions that can be sorted by their
// semantic version.
type ModuleVersionList []*ModuleVersion

func (a ModuleVersionList) Len() int           { return len(a) }
func (a ModuleVersionList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ModuleVersionList) Less(i, j int) bool { return a[i].Version.LessThan(a[j].Version) }

// ModuleUsageHCL builds an example usage of the already parsed Terraform
// module of a particular version, with required providers, a module call and
// an output for each module output.
func (m *ModuleVersion) ModuleUsageHCL(registryFriendlyHostname string) string {
	if m.Module == nil {
		return fmt.Sprintf("No module read for tag %v", m.TagName)
	}

	hclFile := hclwrite.NewEmptyFile()

	// Build terraform block with required provider and terraform versions
	hclFile.Body().AppendBlock(terraformBlock(m.Module))
	hclFile.Body().AppendNewline()

	// Build module call block
	moduleBlock := moduleCallBlock(hclFile.Body(), m, registryFriendlyHostname)
	processModuleVariables(moduleBlock.Body(), m.Module.Variables)

	// Add example outputs
	processModuleOutputs(hclFile.Body(), m.Module.Outputs, moduleBlock.Type(), moduleBlock.Labels()[0])

	var buf bytes.Buffer
	if _, err := hclFile.WriteTo(&buf); err != nil {
		return fmt.Sprintf("error writing HCL to in-memory buffer: %v", err)
	}

	return buf.String()
}

func terraformBlock(m *configs.Module) *hclwrite.Block {
	terraformBlock := hclwrite.NewBlock("terraform", nil)
	if len(m.ProviderRequirements.RequiredProviders) > 0 {
		requiredProvidersBlock := terraformBlock.Body().AppendNewBlock("required_providers", nil)
		requiredProviderKeys := []string{}
		for key := range m.ProviderRequirements.RequiredProviders {
			requiredProviderKeys = append(requiredProviderKeys, key)
		}
		sort.Strings(requiredProviderKeys)
		for _, key := range requiredProviderKeys {
			requiredProvider := m.ProviderRequirements.RequiredProviders[key]
			requiredProvidersBlock.Body().SetAttributeValue(requiredProvider.Name, cty.MapVal(map[string]cty.Value{
				"source":  cty.StringVal(requiredProvider.Source),
				"version": cty.StringVal(requiredProvider.Requirement.Required.String()),
			}))
		}
	}
	terraformBlock.Body().SetAttributeValue("required_version", cty.StringVal(">= 0.14"))
	return terraformBlock
}

func moduleCallBlock(hclFileBody *hclwrite.Body, m *ModuleVersion, registryFriendlyHostname string) *hclwrite.Block {
	moduleCallBlock := hclFileBody.AppendNewBlock("module", []string{m.ModuleRepo.Name})
	moduleCallBlock.Body().SetAttributeValue("source", cty.StringVal(fmt.Sprintf("%s/%s", registryFriendlyHostname, m.ModuleRepo.String())))
	moduleCallBlock.Body().SetAttributeValue("version", cty.StringVal(m.Version.String()))
	if m.Version.Prerelease() == "" {
		constraintString := fmt.Sprintf("~> %d", m.Version.Segments()[0])
		appendComment(moduleCallBlock.Body(), fmt.Sprintf("// or use `version = %q`\n", constraintString))
	}
	return moduleCallBlock
}

func appendComment(body *hclwrite.Body, comment string) {
	body.AppendUnstructuredTokens(hclwrite.Tokens{
		&hclwrite.Token{
			Type:  hclsyntax.TokenComment,
			Bytes: []byte(comment),
		},
	})
}

func processModuleVariables(moduleBlockBody *hclwrite.Body, variables map[string]*configs.Variable) {
	// Collect module input variable names
	requiredVariableNames := []string{}
	optionalVariableNames := []string{}
	for name, variable := range variables {
		switch {
		case variable.Default.IsNull():
			requiredVariableNames = append(requiredVariableNames, name)
		default:
			optionalVariableNames = append(optionalVariableNames, name)
		}
	}
	sort.Strings(requiredVariableNames)
	sort.Strings(optionalVariableNames)

	// List input variables inside module call block, required (no default) variables first, optional variables with default values next
	for _, names := range [][]string{requiredVariableNames, optionalVariableNames} {
		moduleBlockBody.AppendNewline()
		for _, name := range names {
			variable := variables[name]
			if variable.DescriptionSet {
				appendComment(moduleBlockBody, fmt.Sprintf("// %s\n", strings.TrimSuffix(variable.Description, "\n")))
			}
			if variable.Default.IsNull() {
				appendComment(moduleBlockBody, fmt.Sprintf("// type %s\n", variable.Type.FriendlyName()))
			}
			moduleBlockBody.SetAttributeValue(name, variable.Default)
		}
	}
}

func processModuleOutputs(hclFileBody *hclwrite.Body, outputs map[string]*configs.Output, moduleBlockType, moduleBlockLabel string) {
	allOutputs := []string{}
	for name := range outputs {
		allOutputs = append(allOutputs, name)
	}
	sort.Strings(allOutputs)

	for _, name := range allOutputs {
		hclFileBody.AppendNewline()
		outputBlock := hclFileBody.AppendNewBlock("output", []string{name})

		if outputs[name].DescriptionSet {
			appendComment(outputBlock.Body(), fmt.Sprintf("// %s\n", strings.TrimSuffix(outputs[name].Description, "\n")))
		}
		outputBlock.Body().SetAttributeRaw("value", hclwrite.Tokens{
			&hclwrite.Token{
				Type:  hclsyntax.TokenIdent,
				Bytes: []byte(fmt.Sprintf("%s.%s.%s", moduleBlockType, moduleBlockLabel, name)),
			},
		})
		if outputs[name].SensitiveSet {
			outputBlock.Body().SetAttributeValue("sensitive", cty.BoolVal(outputs[name].Sensitive))
		}
	}
}

// EnsureModuleInfo retrieves the files necessary to parse the module at the
// specific version.
func (m *ModuleVersion) EnsureModuleInfo(ctx context.Context, l ListerType, tagName string) error {
	moduleRepo := m.ModuleRepo

	iofs, err := l.RepoTerraformFS(ctx, moduleRepo, tagName)
	if err != nil {
		return err
	}

	fs := afero.FromIOFS{FS: iofs}
	tfParser := configs.NewParser(fs)
	module, diags := tfParser.LoadConfigDir(".")
	if diags.HasErrors() {
		return fmt.Errorf("error loading Terraform config: %v", diags.Error())
	}
	m.Module = module

	readmeContent, err := afero.ReadFile(fs, "README.md")
	if err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("error reading README.md: %v", err)
	}
	m.Readme = string(readmeContent)

	return nil
}

// A ListerType is used to list existing repositories as well as receive new
// module repositories and versions. Additionally, it functions as a download
// proxy for project code.
type ListerType interface {
	Initialize(ctx context.Context) error
	Initialized(ctx context.Context) bool
	GetModuleRepo(namespace, name, provider string) *ModuleRepo
	GetModuleRepos(ctx context.Context) map[string]*ModuleRepo
	GetModuleVersions(ctx context.Context, moduleRepo *ModuleRepo) map[string]*ModuleVersion
	UpsertModuleRepo(ctx context.Context, pathWithNamespace string) (*ModuleRepo, error)
	UpsertModuleVersion(ctx context.Context, moduleRepo *ModuleRepo, tagName string) (*ModuleVersion, error)
	RepoTerraformFS(ctx context.Context, moduleRepo *ModuleRepo, tagName string) (fs.FS, error)
	GenerateDownloadToken(ctx context.Context, namespace, name, provider, version string) (string, error)
	ConsumeDownloadToken(ctx context.Context, token string) (*ModuleVersion, error)
}

// DownloadTokenType holds a module version and a time the token was created.
type DownloadTokenType struct {
	ModuleVersion *ModuleVersion
	Created       time.Time
}

type gitlabLister struct {
	client               *gitlab.Client
	moduleReposLock      sync.RWMutex
	moduleRepos          map[string]*ModuleRepo
	moduleVersionsLock   sync.RWMutex
	moduleVersions       map[string]map[string]*ModuleVersion
	initialized          bool
	modulesGroupFullPath string
	subGroups            bool
	downloadTokens       map[string]DownloadTokenType

	log *logrus.Entry
}

// NewLister creates a new lister that uses the provided GitLab client for
// listing and retrieving project repositories that contain Terraform modules.
func NewLister(client *gitlab.Client, log *logrus.Entry, modulesGroupFullPath string, subGroups bool) ListerType {
	return &gitlabLister{
		client:               client,
		log:                  log,
		moduleRepos:          map[string]*ModuleRepo{},
		moduleVersions:       map[string]map[string]*ModuleVersion{},
		modulesGroupFullPath: modulesGroupFullPath,
		subGroups:            subGroups,
		downloadTokens:       map[string]DownloadTokenType{},
	}
}

func (l *gitlabLister) Initialize(ctx context.Context) error {
	l.moduleReposLock.Lock()
	defer l.moduleReposLock.Unlock()
	l.moduleRepos = map[string]*ModuleRepo{}
	totalPages := -1
	projectsAdded := 0
	for page := 0; page != totalPages; page++ {
		projects, resp, err := l.client.Groups.ListGroupProjects(l.modulesGroupFullPath, &gitlab.ListGroupProjectsOptions{
			Search: gitlab.String("terraform-"),
			Simple: gitlab.Bool(true),
			ListOptions: gitlab.ListOptions{
				Page: page + 1,
			},
			IncludeSubgroups: gitlab.Bool(l.subGroups),
		}, gitlab.WithContext(ctx))
		if err != nil {
			return fmt.Errorf("error listing projects: %v", err)
		}
		totalPages = resp.TotalPages
		for _, project := range projects {
			if err := l.initializeProject(ctx, project.PathWithNamespace); err != nil {
				return err
			}
		}
		projectsAdded += len(projects)
	}
	l.initialized = true
	l.log.Infof("Successfully initialized with %d project(s)", projectsAdded)
	return nil
}

func (l *gitlabLister) initializeProject(ctx context.Context, pathWithNamespace string) error {
	moduleRepo, err := l.projectToModuleRepo(ctx, pathWithNamespace)
	if err != nil {
		return err
	}
	l.log.WithFields(logrus.Fields{
		"namespace": moduleRepo.Namespace,
		"name":      moduleRepo.Name,
		"provider":  moduleRepo.Provider,
		"url":       moduleRepo.URL,
	}).Debug("Adding module repo")
	l.moduleRepos[pathWithNamespace] = moduleRepo
	if err := l.InitModuleVersions(ctx, moduleRepo); err != nil {
		return err
	}
	return nil
}

func (l *gitlabLister) Initialized(ctx context.Context) bool {
	return l.initialized
}

func (l *gitlabLister) GetModuleRepo(namespace, name, provider string) *ModuleRepo {
	l.moduleReposLock.RLock()
	defer l.moduleReposLock.RUnlock()
	for _, r := range l.moduleRepos {
		r := r
		if r.Namespace == namespace && r.Name == name && r.Provider == provider {
			return r
		}
	}
	return nil
}

func (l *gitlabLister) GetModuleRepos(ctx context.Context) map[string]*ModuleRepo {
	l.moduleReposLock.RLock()
	defer l.moduleReposLock.RUnlock()
	return l.moduleRepos
}

func (l *gitlabLister) GetModuleVersions(ctx context.Context, moduleRepo *ModuleRepo) map[string]*ModuleVersion {
	l.moduleVersionsLock.RLock()
	defer l.moduleVersionsLock.RUnlock()
	return l.moduleVersions[moduleRepo.Project.PathWithNamespace]
}

func (l *gitlabLister) projectToModuleRepo(ctx context.Context, pathWithNamespace string) (*ModuleRepo, error) {
	projectPath := path.Base(pathWithNamespace)
	if ModuleProjectName.MatchString(projectPath) {
		matches := ModuleProjectName.FindStringSubmatch(projectPath)
		provider := matches[1]
		project, _, err := l.client.Projects.GetProject(pathWithNamespace, &gitlab.GetProjectOptions{}, gitlab.WithContext(ctx))
		if err != nil {
			return nil, fmt.Errorf("error while looking up project by path with namespace %q: %v", pathWithNamespace, err)
		}
		return &ModuleRepo{
			Namespace: project.Namespace.Path,
			Name:      strings.TrimPrefix(project.Path, fmt.Sprintf("terraform-%s-", provider)),
			Provider:  provider,
			Project:   project,
			URL:       project.WebURL,
		}, nil
	}
	return nil, fmt.Errorf("project name does not match name formula %q", "terraform-<provider>-<name>")
}

func (l *gitlabLister) UpsertModuleRepo(ctx context.Context, pathWithNamespace string) (*ModuleRepo, error) {
	if l.subGroups {
		if !strings.HasPrefix(pathWithNamespace, l.modulesGroupFullPath) {
			return nil, fmt.Errorf("not handling %q as it is not in group %q or one of its sub-groups", pathWithNamespace, l.modulesGroupFullPath)
		}
	} else {
		if path.Dir(pathWithNamespace) != l.modulesGroupFullPath {
			return nil, fmt.Errorf("not handling %q as it is not directly in group %q", pathWithNamespace, l.modulesGroupFullPath)
		}
	}
	var moduleRepo *ModuleRepo
	func() {
		l.moduleReposLock.RLock()
		defer l.moduleReposLock.RUnlock()
		moduleRepo = l.moduleRepos[pathWithNamespace]
	}()
	var err error
	if moduleRepo == nil {
		l.moduleReposLock.Lock()
		defer l.moduleReposLock.Unlock()
		moduleRepo, err = l.projectToModuleRepo(ctx, pathWithNamespace)
		if err != nil {
			return nil, err
		}
		l.moduleRepos[pathWithNamespace] = moduleRepo
	}
	return moduleRepo, nil
}

func (l *gitlabLister) UpsertModuleVersion(ctx context.Context, moduleRepo *ModuleRepo, tagName string) (*ModuleVersion, error) {
	ver, err := version.NewSemver(tagName)
	if err != nil {
		return nil, fmt.Errorf("%q does not constitute a semantic version: %v", tagName, err)
	}

	l.moduleVersionsLock.Lock()
	defer l.moduleVersionsLock.Unlock()

	var moduleVersion *ModuleVersion
	versionsForModuleRepo, exist := l.moduleVersions[moduleRepo.Project.PathWithNamespace]
	if !exist {
		versionsForModuleRepo = map[string]*ModuleVersion{}
		l.moduleVersions[moduleRepo.Project.PathWithNamespace] = versionsForModuleRepo
	} else {
		moduleVersion = versionsForModuleRepo[ver.String()]
	}
	if moduleVersion == nil {

		moduleVersion = &ModuleVersion{
			ModuleRepo: moduleRepo,
			Version:    ver,
			TagName:    tagName,
			WebURL:     fmt.Sprintf("%s/-/archive/%s/%s-%s.zip", moduleRepo.Project.WebURL, tagName, moduleRepo.Project.Path, tagName),
		}
		l.moduleVersions[moduleRepo.Project.PathWithNamespace][ver.String()] = moduleVersion

		if err := moduleVersion.EnsureModuleInfo(ctx, l, tagName); err != nil {
			l.log.WithField("repo", moduleVersion.ModuleRepo.String()).WithField("tag", tagName).Errorf("error ensuring module info is present: %v", err)
		}

		if moduleRepo.LatestVersion == nil || (moduleRepo.LatestVersion.Prerelease() != "" || ver.Prerelease() == "") && ver.GreaterThan(moduleRepo.LatestVersion) {
			l.log.WithFields(map[string]interface{}{
				"module":                  moduleRepo.String(),
				"previous_latest_version": moduleRepo.LatestVersion,
				"latest_version":          moduleVersion.Version,
			}).Info("New latest version")
			moduleRepo.LatestVersion = moduleVersion.Version
		}
	}

	return moduleVersion, nil
}

func (l *gitlabLister) InitModuleVersions(ctx context.Context, r *ModuleRepo) error {
	totalPages := -1
	for page := 0; page != totalPages; page++ {
		tags, resp, err := l.client.Tags.ListTags(r.Project.ID, &gitlab.ListTagsOptions{
			ListOptions: gitlab.ListOptions{
				Page: page + 1,
			},
		}, gitlab.WithContext(ctx))
		if err != nil {
			return fmt.Errorf("error listing tags: %v", err)
		}
		totalPages = resp.TotalPages
		for _, tag := range tags {
			if _, err := version.NewSemver(tag.Name); err != nil {
				continue
			}
			if _, exist := l.moduleVersions[r.Project.PathWithNamespace]; !exist {
				l.moduleVersions[r.Project.PathWithNamespace] = map[string]*ModuleVersion{}
			}
			if _, err := l.UpsertModuleVersion(ctx, r, tag.Name); err != nil {
				return err
			}
		}
	}

	return nil
}

func (l *gitlabLister) RepoTerraformFS(ctx context.Context, moduleRepo *ModuleRepo, tagName string) (fs.FS, error) {
	result := afero.NewMemMapFs()

	totalPages := -1
	for page := 0; page != totalPages; page++ {
		treeNodes, resp, err := l.client.Repositories.ListTree(moduleRepo.Project.ID, &gitlab.ListTreeOptions{
			Ref:       &tagName,
			Recursive: gitlab.Bool(true),
			Path:      gitlab.String("/"),
			ListOptions: gitlab.ListOptions{
				Page: page + 1,
			},
		})
		if err != nil {
			return nil, err
		}
		totalPages = resp.TotalPages
		for _, treeNode := range treeNodes {
			if treeNode.Type == "blob" {
				switch {
				case filepath.Ext(treeNode.Path) == ".tf", filepath.Ext(treeNode.Path) == ".tf.json", treeNode.Path == "README.md":
					if err := downloadGitlabFileToFS(l.client, result, moduleRepo.Project.ID, treeNode.Path, tagName); err != nil {
						return nil, err
					}
				}
			}
		}
	}
	return afero.NewIOFS(result), nil
}

func downloadGitlabFileToFS(client *gitlab.Client, fs afero.Fs, projectID interface{}, path, ref string) error {
	contents, _, err := client.RepositoryFiles.GetRawFile(projectID, path, &gitlab.GetRawFileOptions{
		Ref: &ref,
	})
	if err != nil {
		return err
	}
	return writeFileWithMkDirAll(fs, path, contents)
}

func writeFileWithMkDirAll(fs afero.Fs, path string, contents []byte) error {
	if err := fs.MkdirAll(filepath.Dir(path), 0700); err != nil {
		return err
	}
	f, err := fs.Create(path)
	if err != nil {
		return err
	}
	if _, err := io.Copy(f, bytes.NewReader(contents)); err != nil {
		return err
	}
	return f.Close()
}

func (l *gitlabLister) Logger() *logrus.Entry {
	return l.log
}

func (l *gitlabLister) GenerateDownloadToken(ctx context.Context, namespace, name, provider, version string) (string, error) {
	moduleRepo := l.GetModuleRepo(namespace, name, provider)
	if moduleRepo == nil {
		return "", fmt.Errorf("no such module repo: %s/%s/%s", namespace, name, provider)
	}

	moduleVersions := l.GetModuleVersions(ctx, moduleRepo)
	moduleVersion, exists := moduleVersions[version]
	if !exists {
		return "", fmt.Errorf("no such module version: %s/%s/%s/%s", namespace, name, provider, version)
	}

	// Remove old tokens
	for token, downloadToken := range l.downloadTokens {
		if time.Since(downloadToken.Created) > 5*time.Minute {
			delete(l.downloadTokens, token)
		}
	}

	randomBytes, err := helpers.GenerateRandomBytes(64)
	if err != nil {
		return "", err
	}
	token := url.PathEscape(string(randomBytes))

	// Store token with creation timestamp
	l.downloadTokens[token] = DownloadTokenType{
		ModuleVersion: moduleVersion,
		Created:       time.Now(),
	}

	return token, nil
}

func (l *gitlabLister) ConsumeDownloadToken(ctx context.Context, token string) (*ModuleVersion, error) {
	// Retrieve module version
	downloadToken, exists := l.downloadTokens[token]
	if !exists {
		return nil, fmt.Errorf("no such download token %q", token)
	}

	// Expire token
	delete(l.downloadTokens, token)

	if time.Since(downloadToken.Created) > 5*time.Minute {
		return nil, fmt.Errorf("token has expired")
	}

	// Return module version
	return downloadToken.ModuleVersion, nil
}
