package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"sort"
	"time"

	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"github.com/justinas/nosurf"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/embeds"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/helpers"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/repos"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/routes"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/view"
)

var (
	version       string
	commit        string
	date          string
	ciProjectURL  string
	ciPipelineURL string
)

func main() {
	appConfig := helpers.AppConfig{}
	cliRuntimeHelper := &helpers.CLIRuntimeHelper{
		AppConfig: &appConfig,
	}

	app := initApp()
	app.Before = beforeApp(cliRuntimeHelper)
	app.Flags = flags(&appConfig)

	app.Action = func(c *cli.Context) error {
		e := configureEcho(cliRuntimeHelper)

		lister := repos.NewLister(cliRuntimeHelper.Client(), cliRuntimeHelper.Log(), appConfig.ModulesGroupFullPath, appConfig.SubGroups)

		if err := appConfig.ReadSecrets(); err != nil {
			return fmt.Errorf("error reading secrets: %v", err)
		}
		routes.ConfigureRoutes(cliRuntimeHelper, e, lister)

		go func() {
			if err := lister.Initialize(c.Context); err != nil {
				e.Logger.Fatal("error initializing: %v", err)
			}
		}()

		// Start server
		go func() {
			if err := e.Start(":3000"); err != nil && err != http.ErrServerClosed {
				e.Logger.Fatalf("shutting down the server: %v", err)
			}
		}()

		// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
		// Use a buffered channel to avoid missing signals as recommended for signal.Notify
		quit := make(chan os.Signal, 1)
		signal.Notify(quit, os.Interrupt)
		<-quit
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		if err := e.Shutdown(ctx); err != nil {
			return err
		}
		return nil
	}

	sort.Sort(cli.CommandsByName(app.Commands))
	sort.Sort(cli.FlagsByName(app.Flags))

	if err := app.Run(os.Args); err != nil {
		log.Fatalf("error running app: %+v", err)
	}
}

func initApp() *cli.App {
	app := cli.NewApp()
	app.Usage = "Runs a Terraform Module Registry"
	if version == "" {
		version = "development"
	}
	app.Version = version
	if commit != "" {
		app.Version = fmt.Sprintf("%s (%s)", app.Version, commit)
	}
	app.Metadata = map[string]interface{}{"commit": commit}
	if date != "" {
		dateTime, err := time.Parse(time.RFC3339, date)
		if err != nil {
			log.Fatalf("error parsing date %q: %v", date, err)
		}
		if !dateTime.IsZero() {
			app.Compiled = dateTime
		}
	}
	return app
}

func beforeApp(cliRuntimeHelper *helpers.CLIRuntimeHelper) func(c *cli.Context) error {
	return func(c *cli.Context) error {
		logger := logrus.New()
		log := logger.WithContext(c.Context)
		if cliRuntimeHelper.AppConfig.JSONLog {
			logger.Formatter = &logrus.JSONFormatter{
				TimestampFormat: time.RFC3339,
				FieldMap: logrus.FieldMap{
					logrus.FieldKeyTime:  "@timestamp",
					logrus.FieldKeyLevel: "@level",
					logrus.FieldKeyMsg:   "message",
					logrus.FieldKeyFunc:  "caller",
				},
			}
		}
		switch {
		case cliRuntimeHelper.AppConfig.Trace:
			logger.SetLevel(logrus.TraceLevel)
		case cliRuntimeHelper.AppConfig.Debug:
			logger.SetLevel(logrus.DebugLevel)
		case cliRuntimeHelper.AppConfig.Quiet:
			logger.SetLevel(logrus.ErrorLevel)
		default:
			logger.SetLevel(logrus.InfoLevel)
		}

		cliRuntimeHelper.SetLogger(logger)
		cliRuntimeHelper.SetLog(log)

		token := os.Getenv("GITLAB_TOKEN")
		if path, ok := os.LookupEnv("GITLAB_TOKEN_FILE"); ok {
			content, err := ioutil.ReadFile(path)
			if err != nil {
				return err
			}
			token = string(content)
		}
		opts := []gitlab.ClientOptionFunc{}
		if configuredURL, ok := os.LookupEnv("CI_API_V4_URL"); ok {
			baseURL, err := url.Parse(configuredURL)
			if err != nil {
				return err
			}
			opts = append(opts, gitlab.WithBaseURL(baseURL.String()))
		}
		client, err := gitlab.NewClient(token, opts...)
		if err != nil {
			return fmt.Errorf("creating GitLab client: %+v", err)
		}
		apiURL := *client.BaseURL()
		cliRuntimeHelper.AppConfig.GitlabAPIV4URL = &apiURL
		cliRuntimeHelper.SetClient(client)

		return nil
	}
}

func flags(appConfig *helpers.AppConfig) []cli.Flag {
	return []cli.Flag{
		&cli.BoolFlag{
			Name:        "json-log",
			Destination: &appConfig.JSONLog,
			EnvVars:     []string{"LOG_JSON"},
		},
		&cli.BoolFlag{
			Name:        "trace",
			Usage:       "Enable trace logging",
			Destination: &appConfig.Trace,
		},
		&cli.BoolFlag{
			Name:        "debug",
			Usage:       "Enable debug logging",
			Destination: &appConfig.Debug,
		},
		&cli.BoolFlag{
			Name:        "quiet",
			Usage:       "Only log errors or worse",
			Destination: &appConfig.Quiet,
		},
		&cli.StringFlag{
			Name:        "registry-scheme",
			Usage:       "Scheme for registry URLs such as the /auth redirect URL",
			EnvVars:     []string{"REGISTRY_SCHEME"},
			Destination: &appConfig.RegistryScheme,
			Value:       "https",
		},
		&cli.IntFlag{
			Name:        "registry-port",
			Usage:       "Port for registry URLs such as the /auth redirect URL",
			EnvVars:     []string{"REGISTRY_PORT"},
			Destination: &appConfig.RegistryPort,
			Value:       443,
		},
		&cli.StringFlag{
			Name:        "registry-friendly-hostname",
			Usage:       "Friendly hostname used as prefix for source in module usage",
			EnvVars:     []string{"REGISTRY_FRIENDLY_HOSTNAME"},
			Destination: &appConfig.RegistryFriendlyHostname,
			Required:    true,
		},
		&cli.StringFlag{
			Name:        "group-full-path",
			Usage:       "Full path to the GitLab group in which to search for terraform module projects",
			Aliases:     []string{"p"},
			EnvVars:     []string{"GROUP_FULL_PATH"},
			Required:    true,
			Destination: &appConfig.ModulesGroupFullPath,
		},
		&cli.BoolFlag{
			Name:        "sub-groups",
			Usage:       "Controls whether to search through sub-groups during initialization, and whether or not to accept webhook requests from sub-groups",
			EnvVars:     []string{"SUB_GROUPS"},
			Destination: &appConfig.SubGroups,
		},
		&cli.StringFlag{
			Name:        "minimum-access-level",
			Usage:       "If set, only allow access to members with specified access level directly on the top-level group specified in --group-full-path - valid values are: guest, reporter, developer, maintainer, owner",
			Destination: &appConfig.MinimumAccessLevelName,
		},
	}
}

func configureEcho(cliRuntimeHelper *helpers.CLIRuntimeHelper) *echo.Echo {
	e := echo.New()
	e.HideBanner = true
	e.Debug = cliRuntimeHelper.AppConfig.Debug
	e.IPExtractor = echo.ExtractIPFromXFFHeader()

	e.Use(view.StaticAssets(embeds.Assets, "/assets/", cliRuntimeHelper.Log()))
	sessionKey := string(securecookie.GenerateRandomKey(32))
	e.Use(session.Middleware(sessions.NewCookieStore([]byte(sessionKey))))
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Output: &logFieldsWriter{cliRuntimeHelper.Log(), logrus.InfoLevel, "%s %s", []string{"method", "uri"}},
	}))
	e.Use(middleware.Recover())
	e.Use(middleware.Secure())
	e.Use(middleware.CORS())
	e.Use(middleware.BodyLimit("2M"))
	e.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Level: 5,
	}))
	e.Use(echo.WrapMiddleware(func(h http.Handler) http.Handler {
		n := nosurf.New(h)
		n.ExemptPath("/webhooks")
		n.ExemptPath("/oauth/token")
		n.SetBaseCookie(http.Cookie{
			Secure:   true,
			HttpOnly: true,
			Path:     "/",
			Domain:   cliRuntimeHelper.AppConfig.RegistryFriendlyHostname,
		})
		return n
	}))
	e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Response().Header().Set("X-Terraform-Module-Registry-For-Gitlab-Version", version)
			c.Response().Header().Set("X-Terraform-Module-Registry-For-Gitlab-Commit", commit)
			c.Response().Header().Set("X-Terraform-Module-Registry-For-Gitlab-Date", date)
			c.Response().Header().Set("X-Terraform-Module-Registry-For-Gitlab-CI-Project-URL", ciProjectURL)
			c.Response().Header().Set("X-Terraform-Module-Registry-For-Gitlab-CI-Pipeline-URL", ciPipelineURL)
			err := next(c)
			if err != nil {
				c.Error(err)
			}
			return err
		}
	})

	e.Renderer = view.Engine(e, embeds.ViewsFS, cliRuntimeHelper.AppConfig.RegistryFriendlyHostname, map[string]interface{}{
		"registryFriendlyHostname": cliRuntimeHelper.AppConfig.RegistryFriendlyHostname,
		"version":                  version,
		"commit":                   commit,
		"date":                     date,
		"ciProjectURL":             ciProjectURL,
		"ciPipelineURL":            ciPipelineURL,
	})
	return e
}

type logFieldsWriter struct {
	log    *logrus.Entry
	level  logrus.Level
	format string
	fields []string
}

func (l *logFieldsWriter) Write(p []byte) (n int, err error) {
	m := logrus.Fields{}
	if err := json.Unmarshal(p, &m); err != nil {
		return 0, fmt.Errorf("error unmarshalling log entry as JSON: %v", err)
	}
	args := []interface{}{}
	for _, field := range l.fields {
		args = append(args, m[field])
	}
	l.log.WithFields(m).Log(l.level, fmt.Sprintf(l.format, args...))
	return len(p), nil
}
