package routes_test

import (
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/repos"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/routes"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/testhelpers"
)

func TestHealth(t *testing.T) {
	h, lister, e, server, _, err := testhelpers.SetupSingleRepo(t)
	t.Cleanup(server.Close)
	require.NoError(t, err)

	c, rec := testhelpers.SetupContext(e, http.MethodGet, "/modules/list")
	require.NoError(t, routes.HealthHandler(lister)(c))
	bodyBytes, err := io.ReadAll(rec.Result().Body)
	require.NoError(t, err)
	require.JSONEq(t, `{"status":"UP"}`, string(bodyBytes))

	server, client, _ := testhelpers.SetupServerClientMux(t)
	t.Cleanup(server.Close)
	lister = repos.NewLister(client, h.Log(), "mock-modules", false)
	c, rec = testhelpers.SetupContext(e, http.MethodGet, "/modules/list")
	require.NoError(t, routes.HealthHandler(lister)(c))
	bodyBytes, err = io.ReadAll(rec.Result().Body)
	require.NoError(t, err)
	require.JSONEq(t, `{"status":"DOWN"}`, string(bodyBytes))
}
