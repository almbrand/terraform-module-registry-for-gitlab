package routes

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/helpers"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/repos"
)

// ConfigureRoutes configures the routes for the Module Registry Protocol[1] as
// well as those for navigating the user interface
func ConfigureRoutes(cliRuntimeHelper *helpers.CLIRuntimeHelper, e *echo.Echo, lister repos.ListerType) {
	// Health endpoint
	e.Match([]string{http.MethodGet, http.MethodHead}, "/health", HealthHandler(lister))

	authMiddleware := AuthMiddleware(cliRuntimeHelper.AppConfig)

	// Auth endpoints
	e.GET("/auth", Auth(cliRuntimeHelper.AppConfig))
	e.POST("/sign_in", SignIn(cliRuntimeHelper.AppConfig)).Name = "sign_in"
	e.GET("/sign_out", SignOut).Name = "sign_out"
	e.GET("/oauth/authorize", OAuthAuthorize(cliRuntimeHelper.AppConfig.Oauth2Config()), authMiddleware)
	e.POST("/oauth/token", OAuthToken(cliRuntimeHelper.AppConfig.Oauth2Config()))

	// Module Registry Protcol implementation
	e.GET("/.well-known/terraform.json", Discovery(cliRuntimeHelper.AppConfig.ClientID))
	e.GET("/v1/:namespace/:name/:provider/versions", ModuleVersions(cliRuntimeHelper.AppConfig.GitlabAPIV4URL.String(), lister), authMiddleware)
	e.GET("/v1/:namespace/:name/:provider/:version/download", ModuleDownload(cliRuntimeHelper.AppConfig.GitlabAPIV4URL.String(), lister), authMiddleware)
	e.GET("/download/:token/module.zip", ModuleDownloadProxy(lister))

	// User interface
	e.GET("/", RootRedirect)
	e.GET("/modules/list", ModuleList(cliRuntimeHelper, lister), authMiddleware).Name = "moduleList"
	e.POST("/modules/refresh", ModulesRefresh(cliRuntimeHelper.AppConfig, lister), authMiddleware).Name = "moduleRefresh"
	e.GET("/modules/:namespace/:name/:provider/usage/:version", ModuleUsage(cliRuntimeHelper, lister), authMiddleware).Name = "moduleUsage"

	// Webhooks endpoint
	e.POST("/webhooks", Webhooks(cliRuntimeHelper, lister, cliRuntimeHelper.AppConfig.ValidXGitlabToken))
}
