package routes_test

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"

	"github.com/gorilla/sessions"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/routes"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/testhelpers"
)

const user1ReturnedCode = "RETURNED_CODE_user1"
const user2ReturnedCode = "RETURNED_CODE_user2"
const user3ReturnedCode = "RETURNED_CODE_user3"

func TestAuth(t *testing.T) {
	t.Parallel()

	// Setup
	h, l, e, server, _, err := testhelpers.SetupSingleRepo(t, func(sm *http.ServeMux) {
		sm.HandleFunc("/oauth/token", func(rw http.ResponseWriter, r *http.Request) {
			assert.NoError(t, r.ParseForm())
			code := r.FormValue("code")
			if strings.HasPrefix(code, "RETURNED_CODE_") {
				accessToken := strings.TrimPrefix(code, "RETURNED_CODE_")
				assert.NoError(t, json.NewEncoder(rw).Encode(map[string]interface{}{
					"access_token":  accessToken,
					"token_type":    "bearer",
					"expires_in":    7200,
					"refresh_token": "REFRESH_TOKEN",
					"created_at":    time.Now().Unix(),
				}))
			}
		})
	})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	testCases := []struct {
		code               string
		minAccessLevelName string
		expectErr          bool
	}{
		{
			code: user1ReturnedCode,
		},
		{
			code:      "",
			expectErr: true,
		},
		{
			code:               user1ReturnedCode, // developer
			minAccessLevelName: "developer",
		},
		{
			code:               user1ReturnedCode, // developer
			minAccessLevelName: "maintainer",
			expectErr:          true,
		},
		{
			code:               user2ReturnedCode, // maintainer
			minAccessLevelName: "maintainer",
		},
		{
			code:               user3ReturnedCode, // reporter
			minAccessLevelName: "maintainer",
			expectErr:          true,
		},
	}

	for _, testCase := range testCases {
		h.AppConfig.MinimumAccessLevelName = testCase.minAccessLevelName
		c, rec := testhelpers.SetupContext(e, http.MethodGet, "/auth", func(r *http.Request, _ *httptest.ResponseRecorder) {
			if testCase.code != "" {
				q := r.URL.Query()
				q.Set("code", testCase.code)
				r.URL.RawQuery = q.Encode()
			}
		})
		c.Set("_session_store", sessions.NewCookieStore([]byte("test")))

		err := routes.Auth(h.AppConfig)(c)
		if testCase.expectErr {
			require.Error(t, err, "%#v", testCase)
		} else if assert.NoError(t, err, "%#v", testCase) {
			require.Equal(t, http.StatusFound, rec.Result().StatusCode, "%#v", testCase)

			c, rec := testhelpers.SetupContext(e, http.MethodGet, "/v1/mock-modules/public-module/aws/versions")
			require.NoError(t, routes.ModuleVersions(h.AppConfig.GitlabAPIV4URL.String(), l)(c))
			require.Equal(t, http.StatusOK, rec.Result().StatusCode)
		}
	}
}

func TestSignIn(t *testing.T) {
	t.Parallel()
	// Setup
	h, _, e, server, _, err := testhelpers.Setup(t, testhelpers.MockCase{})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	c, rec := testhelpers.SetupContext(e, http.MethodPost, "/sign_in")

	if assert.NoError(t, testhelpers.Middlewares(h.AppConfig, sessions.NewCookieStore([]byte("SESSION_KEY")), routes.SignIn(h.AppConfig))(c)) {
		require.Equal(t, http.StatusFound, rec.Result().StatusCode)
		require.Contains(t, rec.Result().Header.Get("Location"), fmt.Sprintf("redirect_uri=%s", url.QueryEscape(server.URL)))
	}
}

func TestSignOut(t *testing.T) {
	t.Parallel()
	// Setup
	h, _, e, server, _, err := testhelpers.Setup(t, testhelpers.MockCase{})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	for _, testCase := range []struct {
		query, referer, expectedLocation string
		expectError                      bool
	}{
		{
			query:            "?return=true",
			referer:          "+://x",
			expectedLocation: "/",
			expectError:      true,
		},
		{
			query:            "?return=true",
			referer:          "",
			expectedLocation: "/",
		},
		{
			query:            "?return=true",
			referer:          "/module/list",
			expectedLocation: "/module/list",
		},
		{
			query:            "",
			referer:          "/module/list",
			expectedLocation: "/",
		},
	} {
		c, rec := testhelpers.SetupContext(e, http.MethodPost, fmt.Sprintf("/sign_out%s", testCase.query), func(r *http.Request, rr *httptest.ResponseRecorder) {
			r.Header.Set("Referer", testCase.referer)
		})

		err = testhelpers.Middlewares(h.AppConfig, sessions.NewCookieStore([]byte("SESSION_KEY")), routes.SignOut)(c)
		if testCase.expectError {
			require.Error(t, err)
		} else if assert.NoError(t, err) {
			require.Equal(t, http.StatusFound, rec.Result().StatusCode)
			require.Equal(t, testCase.expectedLocation, rec.Result().Header.Get("Location"))
		}
	}
}

func TestOAuthAuthorize(t *testing.T) {
	t.Parallel()
	// Setup
	h, _, e, server, _, err := testhelpers.Setup(t, testhelpers.MockCase{})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	c, rec := testhelpers.SetupContext(e, http.MethodGet, "/oauth/authorize?client_id=APP_ID&redirect_uri=REDIRECT_URI&response_type=code&state=STATE&scope=REQUESTED_SCOPES&code_challenge=CODE_CHALLENGE&code_challenge_method=S256")

	require.NoError(t, err)
	if assert.NoError(t, routes.OAuthAuthorize(h.AppConfig.Oauth2Config())(c)) {
		require.Equal(t, http.StatusFound, rec.Result().StatusCode)
		require.Contains(t, rec.Result().Header.Get("Location"), server.URL)
		require.Contains(t, rec.Result().Header.Get("Location"), "redirect_uri=REDIRECT_URI")
	}
}

func TestOAuthToken(t *testing.T) {
	t.Parallel()
	// Setup
	h, _, e, server, _, err := testhelpers.Setup(t, testhelpers.MockCase{}, func(sm *http.ServeMux) {
		sm.HandleFunc("/oauth/token", func(rw http.ResponseWriter, r *http.Request) {
			assert.NoError(t, r.ParseForm())
			assert.Equal(t, testhelpers.ClientSecret, r.FormValue("client_secret"))
			assert.NoError(t, json.NewEncoder(rw).Encode(map[string]interface{}{
				"access_token":  "user1",
				"token_type":    "bearer",
				"expires_in":    7200,
				"refresh_token": "REFRESH_TOKEN",
				"created_at":    time.Now().Unix(),
			}))
		})
	})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	q := url.Values{}
	q.Set("client_id", "APP_ID")
	q.Set("code", user1ReturnedCode)
	q.Set("grant_type", "authorization_code")
	q.Set("redirect_uri", "REDIRECT_URI")
	q.Set("code_verifier", "CODE_VERIFIER")
	u := url.URL{
		Path:     "/oauth/token",
		RawQuery: q.Encode(),
	}
	c, rec := testhelpers.SetupContext(e, http.MethodPost, u.String())

	require.NoError(t, err)
	if assert.NoError(t, routes.OAuthToken(h.AppConfig.Oauth2Config())(c)) {
		assert.Equal(t, http.StatusOK, rec.Result().StatusCode)
		body := map[string]interface{}{}
		assert.NoError(t, json.NewDecoder(rec.Result().Body).Decode(&body))
		assert.Equal(t, "user1", body["access_token"])
		assert.Equal(t, "REFRESH_TOKEN", body["refresh_token"])
	}
}
