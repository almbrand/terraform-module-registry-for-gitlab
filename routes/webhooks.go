package routes

import (
	"fmt"
	"net/http"
	"regexp"

	"github.com/hashicorp/go-version"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/helpers"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/repos"
)

var refTagsName = regexp.MustCompile(`refs/tags/(.+)`)

// WebhookPayload represents a payload coming from a GitLab webhook.
//
// See docs at
// https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#tag-events
type WebhookPayload struct {
	ObjectKind string         `json:"object_kind,omitempty"`
	Project    *repos.Project `json:"project,omitempty"`
	Ref        string         `json:"ref,omitempty"`
}

// Webhooks handles a tag_push event from a GitLab webhook.
func Webhooks(h *helpers.CLIRuntimeHelper, l repos.ListerType, validXGitlabToken string) func(c echo.Context) error {
	return webhooksHandler{
		log:               h.Log(),
		l:                 l,
		validXGitlabToken: validXGitlabToken,
	}.handleWebhook
}

type webhooksHandler struct {
	log               *logrus.Entry
	l                 repos.ListerType
	validXGitlabToken string
}

func (w webhooksHandler) handleWebhook(c echo.Context) error {
	if t := c.Request().Header.Get("X-Gitlab-Token"); t != w.validXGitlabToken {
		return echo.NewHTTPError(http.StatusUnauthorized, fmt.Errorf("unexpected X-Gitlab-Token: %q", t))
	}
	xGitlabEvent := c.Request().Header.Get("X-Gitlab-Event")
	switch xGitlabEvent {
	case "Tag Push Hook":
		body := WebhookPayload{}
		if err := c.Bind(&body); err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, fmt.Errorf("error binding webhook body: %v", err))
		}
		ref := body.Ref
		w.log.Infof("Got webhook for ref %v", ref)
		if refTagsName.MatchString(ref) {
			tagName := refTagsName.FindStringSubmatch(ref)[1]
			if _, err := version.NewSemver(tagName); err == nil {
				return w.processWebhookProject(c, body.Project, tagName)
			}
			w.log.Infof("Received %q event with tag %q; not a semantic version.", body.ObjectKind, tagName)
		}
	default:
		w.log.Infof("Unexpected X-Gitlab-Event %q", xGitlabEvent)
	}
	return c.NoContent(http.StatusNoContent)
}

func (w webhooksHandler) processWebhookProject(c echo.Context, project *repos.Project, tagName string) error {
	moduleRepo, err := w.l.UpsertModuleRepo(c.Request().Context(), project.PathWithNamespace)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Errorf("error inserting or updating module repo: %v", err))
	}
	if moduleRepo != nil {
		moduleVersion, err := w.l.UpsertModuleVersion(c.Request().Context(), moduleRepo, tagName)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, fmt.Errorf("error inserting or updating module version: %v", err))
		}
		if moduleVersion != nil {
			w.log.Infof("Updated module version %q for repo %q", moduleVersion.Version.String(), moduleVersion.ModuleRepo.String())
			return c.NoContent(http.StatusOK)
		}
	}
	return nil
}
