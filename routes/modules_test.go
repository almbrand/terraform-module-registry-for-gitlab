package routes_test

import (
	"archive/zip"
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"path"
	"testing"

	"github.com/gorilla/sessions"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/routes"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/testhelpers"
)

func TestModuleList(t *testing.T) {
	t.Parallel()
	// Setup
	h, m, e, server, _, err := testhelpers.SetupMultiRepo(t)
	t.Cleanup(server.Close)
	require.NoError(t, err)

	type testCaseType struct {
		namespace, name, provider string
		invalidAuth               bool
		headers                   map[string]string
		minAccessLevelName        string
	}
	testCases := []testCaseType{
		{
			namespace: "mock-modules",
			name:      "public-module",
			provider:  "aws",
		},
		{
			namespace: "mock-modules",
			name:      "private-module",
			provider:  "aws",
			headers: map[string]string{
				"Authorization": "Bearer user3",
			},
		},
		{
			namespace:          "mock-modules",
			name:               "private-module",
			provider:           "aws",
			minAccessLevelName: "reporter",
			headers: map[string]string{
				"Authorization": "Bearer user5",
			},
		},
		{
			namespace:   "mock-modules",
			name:        "private-module",
			provider:    "aws",
			invalidAuth: true,
			headers: map[string]string{
				"Authorization": fmt.Sprintf("Basic %s", base64.RawURLEncoding.EncodeToString([]byte("AzureDiamond:hunter2"))),
			},
		},
		{
			namespace: "mock-modules",
			name:      "private-module",
			provider:  "aws",
			headers: map[string]string{
				"Authorization": "Bearer user1",
			},
		},
		{
			namespace: "mock-modules",
			name:      "private-module",
			provider:  "aws",
			headers: map[string]string{
				"Authorization": "Bearer user4",
			},
		},
		{
			namespace:   "mock-modules",
			name:        "public-module",
			provider:    "aws",
			invalidAuth: true,
		},
		{
			namespace: "mock-modules",
			name:      "public-module",
			provider:  "aws",
		},
	}

	for _, testCase := range testCases {
		reqFuncs := []func(*http.Request, *httptest.ResponseRecorder){}
		if len(testCase.headers) > 0 {
			reqFuncs = append(reqFuncs, func(r *http.Request, _ *httptest.ResponseRecorder) {
				for name, value := range testCase.headers {
					r.Header.Set(name, value)
				}
			})
		}
		c, rec := testhelpers.SetupContext(e, http.MethodGet, "/modules/list", reqFuncs...)
		h.AppConfig.MinimumAccessLevelName = testCase.minAccessLevelName

		if assert.NoError(t, testhelpers.Middlewares(h.AppConfig, sessions.NewCookieStore([]byte("SESSION_KEY")), routes.ModuleList(h, m))(c), "%#v", testCase) {
			require.Equal(t, http.StatusOK, rec.Result().StatusCode, "%#v", testCase)
			body, err := ioutil.ReadAll(rec.Result().Body)
			require.NoError(t, err)
			if !testCase.invalidAuth {
				require.Contains(t, string(body), testCase.namespace, "%#v", testCase)
				require.Contains(t, string(body), testCase.name, "%#v", testCase)
				require.Contains(t, string(body), testCase.provider, "%#v", testCase)
			}
		}
	}
}

func TestModuleDownload(t *testing.T) {
	t.Parallel()

	h, lister, e, server, _, err := testhelpers.SetupMultiRepo(t)
	t.Cleanup(server.Close)
	require.NoError(t, err)

	type testCaseType struct {
		namespace, name, provider, version string
		expectErr                          bool
		expectedStatusCode                 int
		expectedProxyStatusCode            int
		expectedContentContains            map[string]string
		headers                            map[string]string
	}
	testCases := []testCaseType{
		{
			namespace:          "mock-modules",
			name:               "foo",
			provider:           "bar",
			version:            "1.9.0",
			expectErr:          true,
			expectedStatusCode: http.StatusNotFound,
		},
		{
			namespace:               "mock-modules",
			name:                    "public-module",
			provider:                "aws",
			version:                 "2.3.0",
			expectedStatusCode:      http.StatusNoContent,
			expectedProxyStatusCode: http.StatusOK,
			expectedContentContains: map[string]string{
				"README.md": "# Readme",
				"main.tf":   `"desc"`,
			},
		},
		{
			namespace:          "mock-modules",
			name:               "private-module",
			provider:           "aws",
			version:            "5.6.18",
			expectedStatusCode: http.StatusUnauthorized,
			expectErr:          true,
			headers: map[string]string{
				"Authorization": "Bearer user6",
			},
		},
		{
			namespace:          "mock-modules",
			name:               "private-module",
			provider:           "aws",
			version:            "5.6.18",
			expectErr:          true,
			expectedStatusCode: http.StatusUnauthorized,
			headers: map[string]string{
				"Authorization": fmt.Sprintf("Basic %s", base64.RawURLEncoding.EncodeToString([]byte("AzureDiamond:hunter2"))),
			},
		},
		{
			namespace:               "mock-modules",
			name:                    "private-module",
			provider:                "aws",
			version:                 "5.6.18",
			expectedStatusCode:      http.StatusNoContent,
			expectedProxyStatusCode: http.StatusOK,
			expectedContentContains: map[string]string{
				"README.md": "# Readme",
				"main.tf":   `"desc"`,
			},
			headers: map[string]string{
				"Authorization": "Bearer user1",
			},
		},
		{
			namespace:               "mock-modules",
			name:                    "private-module",
			provider:                "aws",
			version:                 "5.6.18",
			expectedStatusCode:      http.StatusNoContent,
			expectedProxyStatusCode: http.StatusOK,
			expectedContentContains: map[string]string{
				"README.md": "# Readme",
				"main.tf":   `"desc"`,
			},
			headers: map[string]string{
				"Authorization": "Bearer user4",
			},
		},
		{
			namespace:          "mock-modules",
			name:               "public-module",
			provider:           "aws",
			version:            "bogus-version",
			expectErr:          true,
			expectedStatusCode: http.StatusBadRequest,
		},
		{
			namespace:               "mock-modules",
			name:                    "public-module",
			provider:                "aws",
			version:                 "9000.3.0",
			expectedStatusCode:      http.StatusNotFound,
			expectedProxyStatusCode: http.StatusNotFound,
		},
	}

	for i, testCase := range testCases {
		reqFuncs := []func(*http.Request, *httptest.ResponseRecorder){}
		if len(testCase.headers) > 0 {
			reqFuncs = append(reqFuncs, func(r *http.Request, _ *httptest.ResponseRecorder) {
				for name, value := range testCase.headers {
					r.Header.Set(name, value)
				}
			})
		}
		c, rec := testhelpers.SetupContext(e, http.MethodGet, fmt.Sprintf("/v1/%s/%s/%s/%s/download", testCase.namespace, testCase.name, testCase.provider, testCase.version), reqFuncs...)

		err := testhelpers.Middlewares(h.AppConfig, sessions.NewCookieStore([]byte("SESSION_KEY")), routes.ModuleDownload(h.AppConfig.GitlabAPIV4URL.String(), lister))(c)
		if !testCase.expectErr && assert.NoError(t, err, "%d %#v: %v", i, testCase, err) {
			require.Equal(t, testCase.expectedStatusCode, rec.Result().StatusCode, "%#v", testCase)
			body, err := io.ReadAll(rec.Result().Body)
			require.NoError(t, err, "%#v: %v", testCase, err)
			require.Equal(t, testCase.expectedStatusCode, rec.Result().StatusCode, "%#v: %s", testCase, string(body))
			if testCase.expectedStatusCode != http.StatusNoContent {
				continue
			}
			getHeader := rec.Result().Header.Get("X-Terraform-Get")
			u, err := url.Parse(getHeader)
			require.NoError(t, err, "%#v: %v", testCase, err)
			downloadToken := path.Base(path.Dir(u.Path))
			require.NotEmpty(t, downloadToken, "%#v", testCase)

			c, rec := testhelpers.SetupContext(e, http.MethodGet, fmt.Sprintf("/download/%s/module.zip", downloadToken))
			if assert.NoError(t, routes.ModuleDownloadProxy(lister)(c)) {
				require.Equal(t, testCase.expectedProxyStatusCode, rec.Result().StatusCode, "%#v", testCase)
				if testCase.expectedProxyStatusCode != http.StatusOK {
					continue
				}
				contentLength := rec.Result().ContentLength
				require.NotEqual(t, -1, contentLength, "%#v", testCase)
				body, err := ioutil.ReadAll(rec.Result().Body)
				require.NoError(t, err, "%#v: %v", testCase, err)
				zr, err := zip.NewReader(bytes.NewReader(body), contentLength)
				require.NoError(t, err, "%#v: %v", testCase, err)
				for _, zf := range zr.File {
					if zf.FileInfo().IsDir() {
						continue
					}
					f, err := zr.Open(zf.Name)
					require.NoError(t, err, "%#v: %v", testCase, err)
					content, err := ioutil.ReadAll(f)
					require.NoError(t, err, "%#v: %v", testCase, err)
					if contains, shouldCheck := testCase.expectedContentContains[zf.Name]; shouldCheck {
						require.Contains(t, string(content), contains, "%#v", testCase)
					}
				}
			}
		}
	}
}

func TestModuleDownloadProxyBogusToken(t *testing.T) {
	t.Parallel()

	_, lister, e, server, _, err := testhelpers.SetupMultiRepo(t)
	t.Cleanup(server.Close)
	require.NoError(t, err)
	c, _ := testhelpers.SetupContext(e, http.MethodGet, fmt.Sprintf("/download/%s/module.zip", "bogus-token"))
	err = routes.ModuleDownloadProxy(lister)(c)
	if assert.Error(t, err) {
		require.IsType(t, &echo.HTTPError{}, err)
		httpErr := err.(*echo.HTTPError)
		require.Equal(t, http.StatusNotFound, httpErr.Code)
	}
}

func TestModuleUsage(t *testing.T) {
	t.Parallel()
	// Setup
	h, m, e, server, _, err := testhelpers.SetupSingleRepo(t)
	t.Cleanup(server.Close)
	require.NoError(t, err)

	c, rec := testhelpers.SetupContext(e, http.MethodGet, "/modules/mock-modules/public-module/aws/usage/2.3.0")

	if assert.NoError(t, routes.ModuleUsage(h, m)(c)) {
		require.Equal(t, http.StatusOK, rec.Result().StatusCode)
		body, err := ioutil.ReadAll(rec.Result().Body)
		require.NoError(t, err)
		require.Contains(t, string(body), "// desc")
		require.Contains(t, string(body), ">foo<")
		require.Contains(t, string(body), "Readme</h1>")
	}
}

func TestModuleUsageNoMatchingRepo(t *testing.T) {
	t.Parallel()
	// Setup
	h, m, e, server, _, err := testhelpers.Setup(t, testhelpers.MockCase{})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	c, rec := testhelpers.SetupContext(e, http.MethodGet, "/modules/mock-modules/foo/bar/usage/0.0.1+bogus")

	if assert.NoError(t, routes.ModuleUsage(h, m)(c)) {
		require.Equal(t, http.StatusNotFound, rec.Result().StatusCode)
	}
}

func TestModuleVersions(t *testing.T) {
	t.Parallel()
	// Setup
	h, m, e, server, _, err := testhelpers.SetupMultiRepo(t, nil)
	t.Cleanup(server.Close)
	require.NoError(t, err)

	type testCase struct {
		namespace, name, provider string
		expectedVersions          []string
		headers                   map[string]string
	}
	testCases := []testCase{
		{
			namespace: "mock-modules",
			name:      "public-module",
			provider:  "aws",
			expectedVersions: []string{
				"1.2.3",
				"1.2.7",
				"2.1.4",
				"2.3.0",
			},
		},
		{
			namespace: "mock-modules",
			name:      "private-module",
			provider:  "aws",
			expectedVersions: []string{
				"2.3.3",
				"3.1.9",
				"5.6.18",
			},
			headers: map[string]string{
				"Authorization": "Bearer user1",
			},
		},
		{
			namespace: "mock-modules",
			name:      "private-module",
			provider:  "aws",
			expectedVersions: []string{
				"2.3.3",
				"3.1.9",
				"5.6.18",
			},
			headers: map[string]string{
				"Authorization": fmt.Sprintf("Basic %s", base64.RawURLEncoding.EncodeToString([]byte("gitlab-ci-token:CI_JOB_TOKEN_user1"))),
			},
		},
		{
			namespace: "mock-modules",
			name:      "public-module",
			provider:  "azurerm",
			expectedVersions: []string{
				"3.5.13",
				"3.8.2",
			},
		},
	}

	for _, testCase := range testCases {

		reqFuncs := []func(*http.Request, *httptest.ResponseRecorder){}
		if len(testCase.headers) > 0 {
			reqFuncs = append(reqFuncs, func(r *http.Request, _ *httptest.ResponseRecorder) {
				for name, value := range testCase.headers {
					r.Header.Set(name, value)
				}
			})
		}
		c, rec := testhelpers.SetupContext(e, http.MethodGet, fmt.Sprintf("/v1/%s/%s/%s/versions", testCase.namespace, testCase.name, testCase.provider), reqFuncs...)

		if assert.NoError(t, routes.ModuleVersions(h.AppConfig.GitlabAPIV4URL.String(), m)(c), "%#v", testCase) {
			body, err := io.ReadAll(rec.Result().Body)
			require.NoError(t, err, "%#v: %v", testCase, err)
			require.Equal(t, http.StatusOK, rec.Result().StatusCode, "%#v: %s", testCase, string(body))
			response := routes.VersionsResponse{}
			require.NoError(t, json.Unmarshal(body, &response), "%#v: %v", testCase, err)
			modules := response.Modules
			for _, module := range modules {
				require.Equal(t, module.Source, fmt.Sprintf("%s/%s/%s", testCase.namespace, testCase.name, testCase.provider), "%#v", testCase)
				versionsSeen := []string{}
				for _, version := range module.Versions {
					versionsSeen = append(versionsSeen, version.Version)
				}
				require.Equal(t, testCase.expectedVersions, versionsSeen, "%#v", testCase)
			}
		}
	}
}

func TestModuleVersionModuleNotFound(t *testing.T) {
	t.Parallel()
	// Setup
	h, m, e, server, _, err := testhelpers.Setup(t, testhelpers.MockCase{})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	c, rec := testhelpers.SetupContext(e, http.MethodGet, "/v1/mock-repos/foo/bar/versions")

	if assert.NoError(t, routes.ModuleVersions(h.AppConfig.GitlabAPIV4URL.String(), m)(c)) {
		require.Equal(t, http.StatusNotFound, rec.Result().StatusCode)
	}
}

func TestRootRedirect(t *testing.T) {
	t.Parallel()
	// Setup
	_, _, e, server, _, err := testhelpers.SetupSingleRepo(t)
	t.Cleanup(server.Close)
	require.NoError(t, err)

	c, rec := testhelpers.SetupContext(e, http.MethodGet, "/")
	if assert.NoError(t, routes.RootRedirect(c)) {
		require.Equal(t, http.StatusFound, rec.Result().StatusCode)
	}
}

func TestModulesRefresh(t *testing.T) {
	t.Parallel()
	// Setup
	h, m, e, server, _, err := testhelpers.SetupSingleRepo(t)
	t.Cleanup(server.Close)
	require.NoError(t, err)

	store := sessions.NewCookieStore([]byte("SESSION_KEY"))
	c, rec := testhelpers.SetupContext(e, http.MethodPost, "/modules/refresh", func(r *http.Request, rec *httptest.ResponseRecorder) {
		sess, err := store.Get(r, "session")
		require.NoError(t, err)
		require.NoError(t, sess.Save(r, rec))
		r.Header.Set("Authorization", "Bearer user1")
	})
	if assert.NoError(t, testhelpers.Middlewares(h.AppConfig, store, routes.ModulesRefresh(h.AppConfig, m))(c)) {
		require.Equal(t, http.StatusFound, rec.Result().StatusCode)
		require.Equal(t, "/modules/list", rec.Result().Header.Get("Location"))
	}
}

func TestModulesRefreshBadDir(t *testing.T) {
	t.Parallel()
	// Setup
	h, m, e, server, _, err := testhelpers.Setup(t, testhelpers.MockCase{
		FixturesDir:  "invalid_module_fixtures",
		Users:        testhelpers.DefaultUsers,
		GroupMembers: testhelpers.DefaultMembers,
		ModuleRepos: []*testhelpers.MockModuleRepo{
			{
				ProjectID:         1,
				PathWithNamespace: "mock-modules/terraform-aws-public-module",
				Versions: testhelpers.NewMockVersions(t, []*testhelpers.MockModuleVersionInput{
					{
						TagName:     "v2.3.0",
						FS:          testhelpers.InvalidModuleFixtures,
						FixturesDir: "invalid_module_fixtures",
					},
				}),
			},
		},
	})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	c, _ := testhelpers.SetupContext(e, http.MethodPost, "/modules/refresh", func(r *http.Request, rr *httptest.ResponseRecorder) {
		r.Header.Set("Authorization", "Bearer user1")
	})
	require.NoError(t, routes.ModulesRefresh(h.AppConfig, m)(c))
}

func TestDiscovery(t *testing.T) {
	t.Parallel()
	// Setup
	_, _, e, server, _, err := testhelpers.Setup(t, testhelpers.MockCase{})
	t.Cleanup(server.Close)
	require.NoError(t, err)

	c, rec := testhelpers.SetupContext(e, http.MethodGet, "/.well-known/terraform.json")
	if assert.NoError(t, routes.Discovery("tf-cli")(c)) {
		require.Equal(t, http.StatusOK, rec.Result().StatusCode)
		body, err := ioutil.ReadAll(rec.Result().Body)
		require.NoError(t, err)
		require.JSONEq(t, `{"modules.v1": "/v1/","login.v1":{"client":"tf-cli","grant_types":["authz_code"],"authz":"/oauth/authorize","token":"/oauth/token","ports":[10000,10010]}}`, string(body))
	}
}
