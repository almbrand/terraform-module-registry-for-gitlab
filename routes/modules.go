package routes

import (
	"archive/zip"
	"bytes"
	"encoding/base64"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"net/url"
	"os"
	"sort"
	"strings"

	version "github.com/hashicorp/go-version"
	"github.com/justinas/nosurf"
	"github.com/labstack/echo/v4"
	"github.com/spf13/afero"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/helpers"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/repos"
)

// VersionsResponse represents the response to a "List Available Versions for a
// Specific Module" request according to the Module Registry Protocol[1].
//
// [1] https://www.terraform.io/docs/internals/module-registry-protocol.html#list-available-versions-for-a-specific-module
type VersionsResponse struct {
	Modules []Module
}

// Module represents the single module returned, detailing the source address
// to use and the versions available.
type Module struct {
	Source   string
	Versions []Version
}

// Version represents a single version.
type Version struct {
	Version string
}

// ModuleVersions lists the available versions for a specific module.
func ModuleVersions(gitlabBaseURL string, l repos.ListerType) func(c echo.Context) error {
	return func(c echo.Context) error {
		namespace := c.Param("namespace")
		name := c.Param("name")
		provider := c.Param("provider")
		moduleRepo := l.GetModuleRepo(namespace, name, provider)
		if moduleRepo == nil {
			return c.JSON(http.StatusNotFound, ("no module found"))
		}

		if moduleRepo.Project.Visibility != gitlab.PublicVisibility {
			if err := authorizeModuleRepo(c, moduleRepo, gitlab.GuestPermissions, gitlabBaseURL); err != nil {
				return err
			}
		}

		moduleVersions := l.GetModuleVersions(c.Request().Context(), moduleRepo)

		semvers := version.Collection{}
		for _, moduleVersion := range moduleVersions {
			semvers = append(semvers, moduleVersion.Version)
		}
		sort.Sort(semvers)

		versions := []Version{}
		for _, semver := range semvers {
			versions = append(versions, Version{semver.String()})
		}

		source := fmt.Sprintf("%s/%s/%s", namespace, name, provider)

		response := VersionsResponse{
			Modules: []Module{
				{
					Source:   source,
					Versions: versions,
				},
			},
		}

		return c.JSON(http.StatusOK, response)
	}
}

// ModuleDownload returns a module download address in a protocol-specific HTTP
// header, namely "X-Terraform-Get". It directs the Terraform client to the
// download proxy endpoint.
func ModuleDownload(gitlabBaseURL string, l repos.ListerType) func(c echo.Context) error {
	return func(c echo.Context) error {
		namespace, name, provider, versionParam := c.Param("namespace"), c.Param("name"), c.Param("provider"), c.Param("version")
		ver, err := version.NewVersion(versionParam)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, fmt.Errorf("%q does not constitute a semantic version: %v", versionParam, err))
		}

		moduleRepo := l.GetModuleRepo(namespace, name, provider)
		if moduleRepo == nil {
			return echo.NewHTTPError(http.StatusNotFound, fmt.Sprintf("module %q not found", fmt.Sprintf("%s/%s/%s", namespace, name, provider)))
		}

		if moduleRepo.Project.Visibility != gitlab.PublicVisibility {
			if err := authorizeModuleRepo(c, moduleRepo, gitlab.GuestPermissions, gitlabBaseURL); err != nil {
				return err
			}
		}

		if _, exists := l.GetModuleVersions(c.Request().Context(), moduleRepo)[ver.String()]; !exists {
			return c.NoContent(http.StatusNotFound)
		}

		// Generate a token for later download
		downloadToken, err := l.GenerateDownloadToken(c.Request().Context(), namespace, name, provider, ver.String())
		if err != nil {
			return err
		}
		u := url.URL{
			Path: fmt.Sprintf("/download/%s/module.zip", downloadToken),
		}

		// TODO: Consider returning an X-Terraform-Get header value like "git::https://github.com/Azure/terraform-azurerm-compute?ref=v1.2.1" instead.
		// Concerns are that users may need either the ssh:// or https:// endpoints, and we cannot ask the user directly.

		c.Response().Header().Add("X-Terraform-Get", u.String())
		return c.NoContent(http.StatusNoContent)
	}
}

func authorizeTopLevelGroup(c echo.Context, groupFullPath string, minLevel gitlab.AccessLevelValue, gitlabBaseURL string) error {
	user, member, err := groupMemberFromContext(c, groupFullPath, gitlabBaseURL)
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, fmt.Sprintf("error creating GitLab client from context: %v", err))
	} else if user == nil || !user.IsAdmin && (member == nil || member.AccessLevel < minLevel) {
		return echo.NewHTTPError(http.StatusForbidden, "user does not meet required minimum access level")
	}

	return nil
}

func authorizeModuleRepo(c echo.Context, moduleRepo *repos.ModuleRepo, minLevel gitlab.AccessLevelValue, gitlabBaseURL string) error {
	user, member, err := projectMemberFromContext(c, moduleRepo.Project.ID, gitlabBaseURL)
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, fmt.Sprintf("error creating GitLab client from context: %v", err))
	} else if user == nil || !user.IsAdmin && (member == nil || member.AccessLevel < minLevel) {
		return echo.NewHTTPError(http.StatusForbidden, "user does not meet required minimum access level")
	}

	return nil
}

// ErrNoUseableToken is used to indicate no useable token was found in the
// headers or other context fields.
type ErrNoUseableToken error

func gitlabClientFromContext(c echo.Context, gitlabBaseURL string) (*gitlab.Client, *gitlab.User, error) {
	authorizationHeader := c.Request().Header.Get("Authorization")
	contextAccessToken := c.Get("access_token")
	accessToken, accessTokenIsString := contextAccessToken.(string)

	var client *gitlab.Client
	var clientErr error
	switch {
	case strings.HasPrefix(authorizationHeader, "Bearer "):
		client, clientErr = gitlab.NewOAuthClient(strings.TrimPrefix(authorizationHeader, "Bearer "), gitlab.WithBaseURL(gitlabBaseURL))
	case strings.HasPrefix(authorizationHeader, "Basic "):
		basicAuth := strings.TrimPrefix(authorizationHeader, "Basic ")
		creds, err := base64.RawURLEncoding.DecodeString(basicAuth)
		if err != nil {
			return nil, nil, err
		}
		parts := strings.SplitN(string(creds), ":", 2)
		username := parts[0]
		password := ""
		if len(parts) == 2 {
			password = parts[1]
		}
		client, clientErr = gitlab.NewBasicAuthClient(username, password, gitlab.WithBaseURL(gitlabBaseURL))
	case accessTokenIsString && accessToken != "":
		client, clientErr = gitlab.NewOAuthClient(accessToken, gitlab.WithBaseURL(gitlabBaseURL))
	default:
		return nil, nil, ErrNoUseableToken(fmt.Errorf("unable to find a useable token in any headers or the request context"))
	}
	if clientErr != nil {
		return nil, nil, fmt.Errorf("error creating GitLab client: %v", clientErr)
	}

	currentUser, _, err := client.Users.CurrentUser(gitlab.WithContext(c.Request().Context()))
	return client, currentUser, err
}

// ModuleDownloadProxy produces a zip file with the contents of the module repo
// at the given version. The module repo and version are retrieved from a
// download token.
func ModuleDownloadProxy(l repos.ListerType) func(c echo.Context) error {
	return func(c echo.Context) error {
		moduleVersion, err := l.ConsumeDownloadToken(c.Request().Context(), c.Param("token"))
		if err != nil {
			return echo.NewHTTPError(http.StatusNotFound, "download token not found")
		}

		repoTerraformFS, err := l.RepoTerraformFS(c.Request().Context(), moduleVersion.ModuleRepo, moduleVersion.TagName)
		if err != nil {
			return fmt.Errorf("error retrieving repository filesystem: %v", err)
		}
		var buf bytes.Buffer
		iofs := afero.FromIOFS{FS: repoTerraformFS}

		zw := zip.NewWriter(&buf)
		if err := afero.Walk(iofs, ".", func(path string, info fs.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.IsDir() {
				return nil
			}

			f, err := iofs.Open(path)
			if err != nil {
				return fmt.Errorf("error reading %q from in-memory filesystem: %v", path, err)
			}

			w, err := zw.Create(path)
			if err != nil {
				return fmt.Errorf("error creating %q in zip file: %v", path, err)
			}

			if _, err := io.Copy(w, f); err != nil {
				return fmt.Errorf("error copying %q from in-memory filesystem to zip file: %v", path, err)
			}

			return nil
		}); err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("error walking iofs: %v", err))
		}
		if err := zw.Close(); err != nil {
			return fmt.Errorf("error closing and writing zip file to memory: %v", err)
		}
		c.Response().Header().Set("Content-Length", fmt.Sprintf("%d", buf.Len()))

		return c.Stream(http.StatusOK, "application/zip", &buf)
	}
}

// RootRedirect redirects from the root path to the modules list.
func RootRedirect(c echo.Context) error {
	return c.Redirect(http.StatusFound, c.Echo().Reverse("moduleList"))
}

// ModuleList lists the available modules.
func ModuleList(h *helpers.CLIRuntimeHelper, l repos.ListerType) func(c echo.Context) error {
	return func(c echo.Context) error {
		moduleRepos := l.GetModuleRepos(c.Request().Context())
		moduleRepoList := repos.ModuleRepoList{}
		for _, moduleRepo := range moduleRepos {
			if moduleRepo.Project.Visibility != gitlab.PublicVisibility {
				if err := authorizeModuleRepo(c, moduleRepo, gitlab.GuestPermissions, h.Client().BaseURL().String()); err != nil {
					h.Log().WithField("module_repo", moduleRepo.String()).WithField("error", err).Debug("Skipping module repo due to authorization error")
					continue
				}
			}
			moduleRepoList = append(moduleRepoList, moduleRepo)
		}
		sort.Sort(moduleRepoList)

		title := titleWithPrefixFormat(c, "Module list")
		data := echo.Map{
			"title":                    title,
			"token":                    nosurf.Token(c.Request()),
			"reverse":                  c.Echo().Reverse,
			"modules":                  moduleRepoList,
			"registryFriendlyHostname": h.AppConfig.RegistryFriendlyHostname,
		}
		accessToken := c.Get("access_token")
		if accessToken != nil {
			user, member, err := groupMemberFromContext(c, h.AppConfig.ModulesGroupFullPath, h.AppConfig.GitlabAPIV4URL.String())
			if err != nil {
				return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("error getting top level group member: %v", err))
			}
			data["user"] = user
			if (user != nil && user.IsAdmin) || (member != nil && member.AccessLevel >= h.AppConfig.MinimumAccessLevel()) {
				data["showRefresh"] = true
			}
		}

		return c.Render(http.StatusOK, "list", data)
	}
}

// ModuleUsage shows how to use a given module.
func ModuleUsage(h *helpers.CLIRuntimeHelper, l repos.ListerType) func(c echo.Context) error {
	return func(c echo.Context) error {
		moduleRepo := l.GetModuleRepo(c.Param("namespace"), c.Param("name"), c.Param("provider"))
		if moduleRepo == nil {
			return c.NoContent(http.StatusNotFound)
		}

		if moduleRepo.Project.Visibility != gitlab.PublicVisibility {
			if err := authorizeModuleRepo(c, moduleRepo, gitlab.GuestPermissions, h.AppConfig.GitlabAPIV4URL.String()); err != nil {
				return c.NoContent(http.StatusNotFound)
			}
		}

		moduleVersions := l.GetModuleVersions(c.Request().Context(), moduleRepo)
		moduleVersionList := repos.ModuleVersionList{}
		for _, moduleVersion := range moduleVersions {
			moduleVersionList = append(moduleVersionList, moduleVersion)
		}
		sort.Sort(moduleVersionList)

		v := c.Param("version")
		moduleVersion := moduleVersions[v]

		title := titleWithPrefixFormat(c, "%s %s", moduleRepo.String(), moduleVersion.Version.String())
		data := echo.Map{
			"title":                    title,
			"module":                   moduleRepo,
			"version":                  v,
			"latestVersion":            moduleRepo.LatestVersion,
			"moduleVersion":            moduleVersion,
			"moduleVersions":           moduleVersionList,
			"registryFriendlyHostname": h.AppConfig.RegistryFriendlyHostname,
		}
		accessToken := c.Get("access_token")
		if accessToken != nil {
			_, currentUser, err := gitlabClientFromContext(c, h.Client().BaseURL().String())
			if err != nil {
				return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("error looking up current user: %v", err))
			}
			data["user"] = currentUser
		}

		return c.Render(http.StatusOK, "usage", data)
	}
}

// ModulesRefresh refreshes the list of modules by running the initalize method again.
func ModulesRefresh(appConfig *helpers.AppConfig, l repos.ListerType) func(c echo.Context) error {
	return func(c echo.Context) error {
		minAccessLevel := appConfig.MinimumAccessLevel()
		user, member, err := groupMemberFromContext(c, appConfig.ModulesGroupFullPath, appConfig.GitlabAPIV4URL.String())
		if err != nil {
			return echo.NewHTTPError(http.StatusUnauthorized, fmt.Sprintf("error finding user in top level group: %v", err))
		}
		if !user.IsAdmin && member.AccessLevel < minAccessLevel {
			return echo.NewHTTPError(http.StatusUnauthorized, fmt.Sprintf("insufficient access level on group %q", appConfig.ModulesGroupFullPath))
		}

		if member.AccessLevel >= minAccessLevel {
			if err := l.Initialize(c.Request().Context()); err != nil {
				return err
			}
		} else {
			return c.NoContent(http.StatusUnauthorized)
		}
		return c.Redirect(http.StatusFound, "/modules/list")
	}
}

func titleWithPrefixFormat(c echo.Context, prefixFormat string, prefixArgs ...interface{}) string {
	title := fmt.Sprintf("%s - Terraform Module Registry", fmt.Sprintf(prefixFormat, prefixArgs...))
	ciAPIV4URL := os.Getenv("CI_API_V4_URL")
	u, err := url.Parse(ciAPIV4URL)
	if err == nil {
		title = fmt.Sprintf("%s for %s", title, u.Hostname())
	}
	return title
}
