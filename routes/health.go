package routes

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/repos"
)

// HealthHandler is a default handler to serve up
// a health status
func HealthHandler(l repos.ListerType) func(c echo.Context) error {
	return func(c echo.Context) error {
		if l.Initialized(c.Request().Context()) {
			return c.JSON(http.StatusOK, map[string]string{"status": "UP"})
		}
		return c.JSON(http.StatusServiceUnavailable, map[string]string{"status": "DOWN"})
	}
}
