package routes_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/repos"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/routes"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/testhelpers"
)

func TestWebhooks(t *testing.T) {
	t.Parallel()
	// Setup
	h, m, e, server, _, err := testhelpers.SetupSingleRepo(t)
	t.Cleanup(server.Close)
	require.NoError(t, err)

	type testCaseType struct {
		objectKind         string
		project            *repos.Project
		ref                string
		expectedStatusCode int
		expectError        bool
	}
	testCases := []testCaseType{
		{
			objectKind: "tag_push",
			project: &repos.Project{
				PathWithNamespace: "mock-modules/terraform-aws-public-module",
			},
			ref:                "v2.3.0",
			expectedStatusCode: http.StatusOK,
		},
		{
			objectKind: "push",
			project: &repos.Project{
				PathWithNamespace: "mock-modules/terraform-aws-public-module",
			},
			ref:                "deadbeef",
			expectedStatusCode: http.StatusNoContent,
		},
		{
			objectKind: "tag_push",
			project: &repos.Project{
				PathWithNamespace: "mock-modules/terraform-aws-public-module",
			},
			ref:                "non-semantic-version",
			expectedStatusCode: http.StatusNoContent,
		},
		{
			objectKind: "tag_push",
			project: &repos.Project{
				PathWithNamespace: "other-namespace/not-even-a-module-project",
			},
			ref:                "v2.3.0",
			expectError:        true,
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for _, testCase := range testCases {
		var buf bytes.Buffer
		require.NoError(t, json.NewEncoder(&buf).Encode(routes.WebhookPayload{
			ObjectKind: testCase.objectKind,
			Project:    testCase.project,
			Ref:        fmt.Sprintf("refs/tags/%s", testCase.ref),
		}))
		c, rec := testhelpers.SetupContext(e, http.MethodPost, "/webhooks", func(r *http.Request, rr *httptest.ResponseRecorder) {
			r.Body = io.NopCloser(&buf)
			r.ContentLength = int64(buf.Len())
			r.Header.Set("Content-Type", "application/json")
			r.Header.Set("X-Gitlab-Token", h.AppConfig.ValidXGitlabToken)
			r.Header.Set("X-Gitlab-Event", "Tag Push Hook")
		})

		err := routes.Webhooks(h, m, h.AppConfig.ValidXGitlabToken)(c)
		if testCase.expectError {
			require.Error(t, err, "%#v", testCase)
		} else if assert.NoError(t, err, "%#v", testCase) {
			require.Equal(t, testCase.expectedStatusCode, rec.Result().StatusCode, "%#v", testCase)
		}
	}
}
