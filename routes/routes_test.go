package routes_test

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/gorilla/sessions"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/testhelpers"
)

func TestAuthMiddleware(t *testing.T) {
	t.Parallel()

	h, _, e, server, _, err := testhelpers.SetupSingleRepo(t)
	t.Cleanup(server.Close)
	require.NoError(t, err)
	h.AppConfig.MinimumAccessLevelName = "developer"

	type testCaseType struct {
		authorizationHeader          string
		expectedStatusCode           int
		expectedLocationHeaderSuffix string
		expectError                  bool
	}
	testCases := []testCaseType{
		{
			authorizationHeader: "Bearer user1",
			expectedStatusCode:  http.StatusOK,
		},
		{
			authorizationHeader:          "Basic QXp1cmVEaWFtb25kOmh1bnRlcjIK",
			expectedStatusCode:           http.StatusFound,
			expectedLocationHeaderSuffix: "/oauth/authorize",
			expectError:                  true,
		},
	}

	for _, testCase := range testCases {
		c, rec := testhelpers.SetupContext(e, http.MethodGet, "/auth", func(r *http.Request, _ *httptest.ResponseRecorder) {
			if testCase.authorizationHeader != "" {
				r.Header.Set("Authorization", testCase.authorizationHeader)
			}
		})
		err := testhelpers.Middlewares(h.AppConfig, sessions.NewCookieStore([]byte("SESSION_KEY")), echo.HandlerFunc(func(c echo.Context) error {
			if !testCase.expectError {
				accessToken := c.Get("access_token")
				require.NotEmpty(t, accessToken)
				return c.JSON(http.StatusOK, accessToken)
			}
			return echo.NewHTTPError(testCase.expectedStatusCode)
		}))(c)
		if testCase.expectError {
			require.Error(t, err)
		} else if assert.NoError(t, err) {
			require.Equal(t, testCase.expectedStatusCode, rec.Result().StatusCode)
			if testCase.expectedStatusCode == http.StatusOK {
				var authCode string
				require.NoError(t, json.NewDecoder(rec.Result().Body).Decode(&authCode))
				require.Equal(t, testCase.authorizationHeader, fmt.Sprintf("Bearer %s", authCode))
			}
			if testCase.expectedLocationHeaderSuffix != "" {
				u, err := url.Parse(rec.Result().Header.Get("Location"))
				require.NoError(t, err, "%#v", testCase)
				require.Equal(t, testCase.expectedLocationHeaderSuffix, u.Path)

				q := u.Query()
				require.Equal(t, h.AppConfig.Oauth2Config().ClientID, q.Get("client_id"))
				require.Equal(t, h.AppConfig.Oauth2Config().RedirectURL, q.Get("redirect_uri"))
				require.Equal(t, "code", q.Get("response_type"))
				require.Equal(t, strings.Join(h.AppConfig.Oauth2Config().Scopes, " "), q.Get("scope"))
			}
		}
	}
}
