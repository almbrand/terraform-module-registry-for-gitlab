package routes

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func discoveryServices(clientID string) map[string]interface{} {
	return map[string]interface{}{
		"modules.v1": "/v1/",
		"login.v1": map[string]interface{}{
			"client":      clientID,
			"grant_types": []string{"authz_code"},
			"authz":       "/oauth/authorize",
			"token":       "/oauth/token",
			"ports":       []int{10000, 10010},
		},
	}
}

// Discovery lists the supported services and what prefix to use for requests.
func Discovery(clientID string) func(c echo.Context) error {
	return func(c echo.Context) error {
		return c.JSON(http.StatusOK, discoveryServices(clientID))
	}
}
