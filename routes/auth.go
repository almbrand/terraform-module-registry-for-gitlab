package routes

import (
	"encoding/gob"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/gorilla/sessions"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/helpers"
	"golang.org/x/oauth2"
)

// Auth handles users returning from GitLab's /oauth/authorize endpoint.
func Auth(appConfig *helpers.AppConfig) func(c echo.Context) error {
	return auther{appConfig: appConfig}.auth
}

type auther struct {
	appConfig *helpers.AppConfig
}

func (a auther) auth(c echo.Context) error {
	accessToken, err := responseCodeToAccessToken(c, a.appConfig)
	if err != nil {
		return err
	}
	c.Set("access_token", accessToken)

	redirectPath := "/"
	if err := doWithSession(c, func(sess *sessions.Session) {
		sess.Values["access_token"] = accessToken
		if v, exists := sess.Values["redirect_path"]; exists {
			redirectPath = v.(string)
			delete(sess.Values, "redirect_path")
		}
	}); err != nil {
		return err
	}

	// Require top level group access level if set
	if a.appConfig.MinimumAccessLevel() > gitlab.NoPermissions {
		if err := authorizeTopLevelGroup(c, a.appConfig.ModulesGroupFullPath, a.appConfig.MinimumAccessLevel(), a.appConfig.GitlabAPIV4URL.String()); err != nil {
			return echo.NewHTTPError(http.StatusUnauthorized, fmt.Sprintf("insufficient access level on group %q", a.appConfig.ModulesGroupFullPath))
		}
	}

	return c.Redirect(http.StatusFound, redirectPath)
}

func doWithSession(c echo.Context, funcs ...func(*sessions.Session)) error {
	// Retrieve session
	sess, err := session.Get("session", c)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "error getting session: %v", err)
	}
	for _, f := range funcs {
		f(sess)
	}
	if err := sess.Save(c.Request(), c.Response()); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("error saving session: %v", err))
	}
	return nil
}

func responseCodeToAccessToken(c echo.Context, appConfig *helpers.AppConfig) (string, error) {
	code := c.QueryParam("code")

	if code == "" {
		return "", echo.NewHTTPError(http.StatusBadRequest, "query parameter `code` is missing")
	}

	response, err := postCodeForOAuth2Token(appConfig, code)
	if err != nil {
		return "", echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("error decoding authorization response: %v", err))
	}
	return response.AccessToken, nil
}

func postCodeForOAuth2Token(appConfig *helpers.AppConfig, code string) (oauth2.Token, error) {
	result := oauth2.Token{}
	u := url.URL{
		Host:   appConfig.GitlabAPIV4URL.Host,
		Scheme: appConfig.GitlabAPIV4URL.Scheme,
		Path:   "/oauth/token",
	}
	data := url.Values{}
	config := appConfig.Oauth2Config()
	data.Set("client_id", config.ClientID)
	data.Set("client_secret", config.ClientSecret)
	data.Set("grant_type", "authorization_code")
	data.Set("code", code)
	data.Set("redirect_uri", config.RedirectURL)
	data.Set("scope", strings.Join(config.Scopes, " "))
	resp, err := http.PostForm(u.String(), data)
	if err != nil {
		return result, echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("error during authorization request: %v", err))
	}
	if resp.StatusCode != http.StatusOK {
		errorResponseBody, _ := ioutil.ReadAll(resp.Body)
		return result, echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("unexpected status code %v during token request: %v", resp.StatusCode, string(errorResponseBody)))
	}

	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return result, echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("error decoding authorization response: %v", err))
	}
	return result, nil
}

func projectMemberFromContext(c echo.Context, projectID interface{}, gitlabBaseURL string) (*gitlab.User, *gitlab.ProjectMember, error) {
	client, user, err := gitlabClientFromContext(c, gitlabBaseURL)
	if err != nil {
		if _, ok := err.(ErrNoUseableToken); !ok {
			return nil, nil, fmt.Errorf("error creating client for authenticated user: %v", err)
		}
		return user, nil, err
	}

	member, projectMemberResponse, err := client.ProjectMembers.GetInheritedProjectMember(projectID, user.ID, gitlab.WithContext(c.Request().Context()))
	if err != nil {
		if projectMemberResponse == nil || projectMemberResponse.StatusCode != http.StatusNotFound {
			return nil, nil, echo.NewHTTPError(http.StatusUnauthorized, fmt.Sprintf("error finding user in project %q: %v", projectID, err))
		}
	}
	return user, member, nil
}

func groupMemberFromContext(c echo.Context, groupFullPath string, gitlabBaseURL string) (*gitlab.User, *gitlab.GroupMember, error) {
	client, user, err := gitlabClientFromContext(c, gitlabBaseURL)
	if err != nil {
		if _, ok := err.(ErrNoUseableToken); !ok {
			return nil, nil, fmt.Errorf("error creating client for authenticated user: %v", err)
		}
		return user, nil, err
	}

	member, groupMemberResponse, err := client.GroupMembers.GetGroupMember(groupFullPath, user.ID, gitlab.WithContext(c.Request().Context()))
	if err != nil {
		if groupMemberResponse == nil || groupMemberResponse.StatusCode != http.StatusNotFound {
			return nil, nil, echo.NewHTTPError(http.StatusUnauthorized, fmt.Sprintf("error finding user in top level group: %v", err))
		}
	}
	return user, member, nil
}

// SignIn redirects the user to log in.
func SignIn(appConfig *helpers.AppConfig) func(c echo.Context) error {
	return func(c echo.Context) error {
		// Retrieve session
		sess, err := session.Get("session", c)
		if err != nil {
			redirectPath := "/sign_out?return=true"
			return c.Redirect(http.StatusFound, redirectPath)
		}

		referer, err := refererOrRoot(c)
		if err != nil {
			return fmt.Errorf("error building redirect path from referer URL: %v", err)
		}

		sess.Values["redirect_path"] = referer
		return redirectToAuthorize(c, sess, appConfig.Oauth2Config())
	}
}

// SignOut clears the session cookie and redirects back to the referer.
func SignOut(c echo.Context) error {
	c.SetCookie(&http.Cookie{
		Name:    "session",
		Expires: time.Time{},
		MaxAge:  -1,
		Value:   "",
	})

	location := "/"
	if c.QueryParam("return") == "true" {
		var err error
		location, err = refererOrRoot(c)
		if err != nil {
			return fmt.Errorf("error building redirect path from referer URL: %v", err)
		}
	}

	return c.Redirect(http.StatusFound, location)
}

func refererOrRoot(c echo.Context) (string, error) {
	locationURL := &url.URL{
		Path: "/",
	}
	referer := c.Request().Header.Get("Referer")
	if referer != "" {
		var err error
		locationURL, err = url.Parse(referer)
		if err != nil {
			return "", err
		}
	}
	if referer == "" || strings.HasPrefix(locationURL.Path, "/sign_out") {
		locationURL.Path = "/"
		locationURL.RawQuery = ""
	}
	locationURL.Scheme = ""
	locationURL.Host = ""
	return locationURL.String(), nil
}

// OAuthAuthorize handles the GET /oauth/authorize request by redirecting the
// user to the corresponding GitLab authorization endpoint.
func OAuthAuthorize(config oauth2.Config) func(c echo.Context) error {
	return func(c echo.Context) error {
		u, err := url.Parse(config.Endpoint.AuthURL)
		if err != nil {
			return err
		}
		u.RawQuery = c.Request().URL.RawQuery
		u.Query().Set("client_secret", config.ClientSecret)
		return c.Redirect(http.StatusFound, u.String())
	}
}

// OAuthToken handles the POST /oauth/token request by adding the client_secret
// parameter to the request before making the same request to the given GitLab
// host and proxying the response to the original client.
func OAuthToken(config oauth2.Config) func(c echo.Context) error {
	return func(c echo.Context) error {
		u, err := url.Parse(config.Endpoint.TokenURL)
		if err != nil {
			return err
		}
		vals, err := c.FormParams()
		if err != nil {
			return err
		}
		vals.Set("client_secret", config.ClientSecret)
		resp, err := http.PostForm(u.String(), vals)
		if err != nil {
			return fmt.Errorf("error posting request: %v", err)
		}
		if resp.StatusCode != http.StatusOK {
			return fmt.Errorf("unexpected status %v", resp.StatusCode)
		}

		token := oauth2.Token{}
		if err := json.NewDecoder(resp.Body).Decode(&token); err != nil {
			return fmt.Errorf("error decoding response body from GitLab token endpoint: %v", err)
		}
		return c.JSON(http.StatusOK, token)
	}
}

// AuthMiddleware builds an echo.MiddlewareFunc which redirects unauthorized
// requests to a GitLab authorization page.
func AuthMiddleware(appConfig *helpers.AppConfig) echo.MiddlewareFunc {
	gob.Register(gitlab.AccessLevelValue(0))
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			var accessToken string

			authorizationHeader := c.Request().Header.Get("Authorization")
			if strings.HasPrefix(authorizationHeader, "Bearer ") {
				accessToken = strings.TrimPrefix(authorizationHeader, "Bearer ")
				c.Set("access_token", accessToken)
			} else {
				accessToken, redirect := redirectIfNoAccessTokenInSession(c, appConfig)
				if redirect != nil {
					return redirect
				} else if accessToken != "" {
					c.Set("access_token", accessToken)
				}
			}

			err := next(c)
			if err != nil {
				c.Error(err)
			}
			return err
		}
	}
}

func redirectIfNoAccessTokenInSession(c echo.Context, appConfig *helpers.AppConfig) (string, error) {
	// Retrieve session
	sess, err := session.Get("session", c)
	if err != nil {
		redirectPath := "/sign_out?return=true"
		if strings.HasPrefix(c.Path(), "/sign_out") {
			redirectPath = "/"
		}
		return "", c.Redirect(http.StatusFound, redirectPath)
	}
	tokenRaw, exists := sess.Values["access_token"]
	if !exists && appConfig.MinimumAccessLevel() > gitlab.NoPermissions {
		return "", redirectToAuthorize(c, sess, appConfig.Oauth2Config())
	}
	if token, ok := tokenRaw.(string); ok {
		return token, nil
	}
	return "", nil
}

func redirectToAuthorize(c echo.Context, sess *sessions.Session, config oauth2.Config) error {
	if _, exists := sess.Values["redirect_path"]; !exists {
		sess.Values["redirect_path"] = c.Request().URL.Path
		if err := sess.Save(c.Request(), c.Response()); err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("error saving session: %v", err))
		}
	}

	q := url.Values{}
	q.Set("client_id", config.ClientID)
	q.Set("redirect_uri", config.RedirectURL)
	q.Set("response_type", "code")
	q.Set("scope", strings.Join(config.Scopes, " "))
	authURL, err := url.Parse(config.Endpoint.AuthURL)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("unable to parse AuthURL %q: %v", config.Endpoint.AuthURL, err))
	}
	authURL.RawQuery = q.Encode()

	return c.Redirect(http.StatusFound, authURL.String())
}
