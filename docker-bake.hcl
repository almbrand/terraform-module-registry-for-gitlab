variable "DOCKER_IMAGE_TAG" {
  default = "local"
}

variable "CI_REGISTRY_IMAGE" {
  default = "registry.gitlab.com/almbrand/terraform-module-registry-for-gitlab"
}

variable "CI_PROJECT_NAME" {
  default = "terraform-module-registry-for-gitlab"
}

group "default" {
  targets = ["app"]
}

target "app" {
  args = {
    CI_PROJECT_NAME = "${CI_PROJECT_NAME}"
  }
  tags      = ["${CI_REGISTRY_IMAGE}:${DOCKER_IMAGE_TAG}"]
  platforms = ["linux/amd64"]
}
