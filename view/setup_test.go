package view_test

import (
	"bytes"
	"context"
	"net/http"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/embeds"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/testhelpers"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/view"
)

func TestEngine(t *testing.T) {
	_, lister, e, server, _, err := testhelpers.SetupSingleRepo(t)
	t.Cleanup(server.Close)
	require.NoError(t, err)

	engine := view.Engine(e, embeds.ViewsFS, "example.com", map[string]interface{}{"version": "test"})
	var buf bytes.Buffer
	c, _ := testhelpers.SetupContext(e, http.MethodGet, "/modules/list")
	require.NoError(t, engine.Render(&buf, "list", echo.Map{}, c))

	moduleRepo := lister.GetModuleRepo("mock-modules", "public-module", "aws")
	moduleVersion := lister.GetModuleVersions(context.Background(), moduleRepo)["2.3.0"]
	require.NoError(t, engine.Render(&buf, "usage", echo.Map{
		"moduleVersion": moduleVersion,
	}, c))
	require.Error(t, engine.Render(&buf, "non-existing-page", echo.Map{}, c))
}
