package view_test

import (
	"io"
	"net/http"
	"testing"

	svg "github.com/h2non/go-is-svg"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/routes"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/testhelpers"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/view"
)

func TestStaticAssets(t *testing.T) {
	t.Parallel()
	_, _, e, server, _, _ := testhelpers.Setup(t, testhelpers.MockCase{})
	t.Cleanup(server.Close)
	noop := func(c echo.Context) error {
		return nil
	}
	mw := view.StaticAssets(testhelpers.AssetFixtures, "/asset_fixtures/", logrus.NewEntry(logrus.StandardLogger()))
	c, rec := testhelpers.SetupContext(e, http.MethodGet, "/asset_fixtures/asset.txt")
	if assert.NoError(t, mw(noop)(c)) {
		require.Equal(t, http.StatusOK, rec.Result().StatusCode)
		bodyBytes, err := io.ReadAll(rec.Result().Body)
		require.NoError(t, err)
		require.Equal(t, "foo", string(bodyBytes))
	}
	c, rec = testhelpers.SetupContext(e, http.MethodGet, "/asset_fixtures/gitlab-logo-1-color-black-rgb")
	if assert.NoError(t, mw(noop)(c)) {
		require.Equal(t, http.StatusOK, rec.Result().StatusCode)
		bodyBytes := make([]byte, rec.Result().ContentLength)
		_, err := io.ReadFull(rec.Result().Body, bodyBytes)
		require.NoError(t, err)
		require.True(t, svg.Is(bodyBytes))
		f, err := testhelpers.AssetFixtures.Open("asset_fixtures/gitlab-logo-1-color-black-rgb")
		require.NoError(t, err)
		realBytes, err := io.ReadAll(f)
		require.NoError(t, err)
		require.NoError(t, f.Close())
		require.Equal(t, string(realBytes), string(bodyBytes))
	}

	c, _ = testhelpers.SetupContext(e, http.MethodGet, "/asset_fixtures/non-existing-file")
	err := mw(noop)(c)
	require.Error(t, err)
	require.IsType(t, &echo.HTTPError{}, err)
	require.Equal(t, http.StatusNotFound, err.(*echo.HTTPError).Code)

	c, _ = testhelpers.SetupContext(e, http.MethodGet, "/asset_fixtures/subdir")
	err = mw(noop)(c)
	require.Error(t, err)
	require.IsType(t, &echo.HTTPError{}, err)
	require.Equal(t, http.StatusNotFound, err.(*echo.HTTPError).Code)

	c, rec = testhelpers.SetupContext(e, http.MethodGet, "/")
	require.NoError(t, mw(routes.RootRedirect)(c))
	require.Equal(t, http.StatusFound, rec.Result().StatusCode)
}
