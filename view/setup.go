package view

import (
	"bytes"
	"fmt"
	"html/template"
	"io/fs"
	"path/filepath"

	"github.com/alecthomas/chroma/formatters/html"
	"github.com/alecthomas/chroma/lexers"
	"github.com/alecthomas/chroma/styles"
	"github.com/foolin/goview"
	"github.com/foolin/goview/supports/echoview-v4"
	"github.com/gomarkdown/markdown"
	mdhtml "github.com/gomarkdown/markdown/html"
	"github.com/gomarkdown/markdown/parser"
	"github.com/labstack/echo/v4"
)

// Engine configures a view engine based on a provided filesystem and adds
// functions for syntax highlighting and accessing named routes.
func Engine(e *echo.Echo, templateFS fs.ReadFileFS, registryFriendlyHostname string, buildMeta map[string]interface{}) *echoview.ViewEngine {
	evConfig := goview.DefaultConfig
	evConfig.Master = filepath.Join("layouts", "main")
	evConfig.Extension = ".html.gtpl"
	evConfig.Funcs = template.FuncMap{
		"reverse":                  e.Reverse,
		"markdown":                 templateMarkdown,
		"chroma":                   templateChroma,
		"chromacss":                templateChromaCSS,
		"registryFriendlyHostname": templateRegistryFriendlyHostname(registryFriendlyHostname),
		"buildMeta":                templateBuildMeta(buildMeta),
	}

	ev := echoview.New(evConfig)
	ev.SetFileHandler(fsFileHandler(templateFS))

	return ev
}

func templateMarkdown(input string) template.HTML {
	return template.HTML(markdown.ToHTML([]byte(input), parser.NewWithExtensions(parser.CommonExtensions), mdhtml.NewRenderer(mdhtml.RendererOptions{})))
}

func templateChroma(input string, lexerName string, styleName string) template.HTML {
	var buf bytes.Buffer
	formatter := html.New(html.WithClasses(true), html.PreventSurroundingPre(true))
	lexer := lexers.Get(lexerName)
	iterator, err := lexer.Tokenise(nil, input)
	if err != nil {
		return template.HTML(fmt.Sprintf("unable to lex code: %v", err))
	}
	if err := formatter.Format(&buf, styles.Get(styleName), iterator); err != nil {
		return template.HTML(fmt.Sprintf("unable to format code: %v", err))
	}
	return template.HTML(buf.String())
}

func templateChromaCSS(styleName string) template.CSS {
	var buf bytes.Buffer
	if err := html.New(html.WithClasses(true)).WriteCSS(&buf, styles.Get(styleName)); err != nil {
		return template.CSS(fmt.Sprintf("error writing CSS to in-memory buffer: %v", err))
	}
	return template.CSS(buf.String())
}

func templateRegistryFriendlyHostname(registryFriendlyHostname string) func() string {
	return func() string {
		return registryFriendlyHostname
	}
}

func fsFileHandler(fs fs.ReadFileFS) func(goview.Config, string) (string, error) {
	return func(config goview.Config, tplFile string) (content string, err error) {
		// Get the path of the template from the root
		path := filepath.Join(config.Root, tplFile+config.Extension)
		data, err := fs.ReadFile(path)
		if err != nil {
			return "", fmt.Errorf("ViewEngine render read name:%v, path:%v, error: %v", tplFile, path, err)
		}
		return string(data), nil
	}
}

func templateBuildMeta(buildMeta map[string]interface{}) func(string) interface{} {
	return func(key string) interface{} {
		return buildMeta[key]
	}
}
