package view

import (
	"bytes"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/h2non/filetype"
	"github.com/h2non/filetype/types"
	svg "github.com/h2non/go-is-svg"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"github.com/spf13/afero"
)

var (
	svgType = filetype.AddType(".svg", "image/svg+xml")
)

// StaticAssets serves any files under the given assets root from a given
// fs.FS filesystem.
func StaticAssets(assetsFS fs.FS, assetsRoot string, log *logrus.Entry) func(next echo.HandlerFunc) echo.HandlerFunc {
	iofs := afero.NewIOFS(afero.FromIOFS{FS: assetsFS})
	filetype.AddType(".svgz", "image/svg+xml")
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			if strings.HasPrefix(c.Path(), assetsRoot) {
				if err := handleAsset(c, iofs, log); err != nil {
					return err
				}
			}

			err := next(c)
			if err != nil {
				c.Error(err)
			}
			return err
		}
	}
}

func handleAsset(c echo.Context, iofs afero.IOFS, log *logrus.Entry) error {
	assetPath := strings.TrimPrefix(c.Path(), "/")
	f, size, err := openAssetFile(c, iofs, assetPath)
	if err != nil {
		return err
	}

	fType, err := readAssetFileForFileType(f, assetPath)
	if err != nil {
		return err
	}
	defer func() {
		err := f.Close()
		if err != nil {
			log.Errorf("error closing file: %v", err)
		}
	}()

	c.Response().Header().Set("Content-Length", fmt.Sprintf("%d", size))
	if _, err := f.Seek(0, 0); err != nil {
		return err
	}
	return c.Stream(http.StatusOK, fType.MIME.Value, f)
}

func openAssetFile(c echo.Context, iofs afero.IOFS, assetPath string) (afero.File, int64, error) {
	stat, err := statAssetFile(c, iofs, assetPath)
	if err != nil {
		return nil, 0, err
	}
	f, err := afero.FromIOFS{FS: iofs}.Open(assetPath)
	if err != nil {
		return nil, 0, echo.NewHTTPError(http.StatusInternalServerError, err)
	}
	return f, stat.Size(), nil
}

func statAssetFile(c echo.Context, iofs afero.IOFS, assetPath string) (fs.FileInfo, error) {
	stat, err := iofs.Stat(assetPath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, echo.NewHTTPError(http.StatusNotFound)
		}
		return nil, echo.NewHTTPError(http.StatusInternalServerError, err)
	}
	if stat.IsDir() {
		return nil, echo.NewHTTPError(http.StatusNotFound)
	}
	return stat, nil
}

func readAssetFileForFileType(f io.Reader, assetPath string) (types.Type, error) {
	var buf bytes.Buffer
	fType := types.Get(filepath.Ext(filepath.Base(assetPath)))
	tr := io.TeeReader(f, &buf)
	if fType == filetype.Unknown {
		var err error
		fType, err = filetype.MatchReader(tr)
		if err != nil {
			fType = types.Unknown
		}
		if fType == types.Unknown {
			if _, err := io.ReadAll(tr); err != nil {
				return types.Type{}, echo.NewHTTPError(http.StatusInternalServerError, err)
			}
			if svg.Is(buf.Bytes()) {
				fType = svgType
			}
		}
	}
	return fType, nil
}
