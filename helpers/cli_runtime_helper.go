package helpers

import (
	"fmt"
	"io/ioutil"
	"net/url"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"golang.org/x/oauth2"
)

// AppConfig ...
type AppConfig struct {
	JSONLog bool
	DryRun  bool
	Debug   bool
	Trace   bool
	Quiet   bool

	GitlabAPIV4URL           *url.URL
	RegistryScheme           string
	RegistryPort             int
	RegistryFriendlyHostname string
	ModulesGroupFullPath     string
	MinimumAccessLevelName   string
	SubGroups                bool

	ValidXGitlabToken string
	ClientID          string
	ClientSecret      string
}

// ReadSecrets reads secrets either from environment or files.
func (a *AppConfig) ReadSecrets() error {
	a.ValidXGitlabToken = os.Getenv("VALID_X_GITLAB_TOKEN")
	if path, ok := os.LookupEnv("VALID_X_GITLAB_TOKEN_FILE"); ok {
		content, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}
		a.ValidXGitlabToken = string(content)
	}

	a.ClientID = os.Getenv("OAUTH_CLIENT_ID")
	if path, ok := os.LookupEnv("OAUTH_CLIENT_ID_FILE"); ok {
		content, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}
		a.ClientID = string(content)
	}
	a.ClientSecret = os.Getenv("OAUTH_CLIENT_SECRET")
	if path, ok := os.LookupEnv("OAUTH_CLIENT_SECRET_FILE"); ok {
		content, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}
		a.ClientSecret = string(content)
	}
	return nil
}

// Oauth2Config returns the redirect URL for OAuth2 use.
func (a *AppConfig) Oauth2Config() oauth2.Config {
	// Build redirect URL
	host := a.RegistryFriendlyHostname
	switch {
	case a.RegistryScheme == "https" && a.RegistryPort != 443,
		a.RegistryScheme == "http" && a.RegistryPort != 80:
		host = fmt.Sprintf("%s:%d", host, a.RegistryPort)
	}

	redirectURL := url.URL{
		Scheme: a.RegistryScheme,
		Host:   host,
		Path:   "/auth",
	}

	authURL := *a.GitlabAPIV4URL
	authURL.Path = "/oauth/authorize"
	tokenURL := *a.GitlabAPIV4URL
	tokenURL.Path = "/oauth/token"

	return oauth2.Config{
		ClientID:     a.ClientID,
		ClientSecret: a.ClientSecret,
		Endpoint: oauth2.Endpoint{
			AuthStyle: oauth2.AuthStyleInParams,
			AuthURL:   authURL.String(),
			TokenURL:  tokenURL.String(),
		},
		RedirectURL: redirectURL.String(),
		Scopes:      []string{"api"},
	}
}

// MinimumAccessLevel returns a gitlab.AccessLevelValue based on the string
// flag value MinimumAccessLevelName.
func (a *AppConfig) MinimumAccessLevel() gitlab.AccessLevelValue {
	switch a.MinimumAccessLevelName {
	case "guest":
		return gitlab.GuestPermissions
	case "reporter":
		return gitlab.ReporterPermissions
	case "developer":
		return gitlab.DeveloperPermissions
	case "maintainer":
		return gitlab.MaintainerPermissions
	case "owner":
		return gitlab.OwnerPermissions
	default:
		return gitlab.NoPermissions
	}
}

// CLIRuntimeHelper contains common CLI runtime facilities
type CLIRuntimeHelper struct {
	client *gitlab.Client
	logger *logrus.Logger
	log    *logrus.Entry

	AppConfig *AppConfig
}

// SetClient sets the client
func (h *CLIRuntimeHelper) SetClient(v *gitlab.Client) {
	h.client = v
}

// SetLogger sets the logger
func (h *CLIRuntimeHelper) SetLogger(v *logrus.Logger) {
	h.logger = v
}

// SetLog sets the log
func (h *CLIRuntimeHelper) SetLog(v *logrus.Entry) {
	h.log = v
}

// Client returns the GitLab client
func (h *CLIRuntimeHelper) Client() *gitlab.Client {
	return h.client
}

// Logger returns the logger object, in order to check if a logging level is set.
func (h *CLIRuntimeHelper) Logger() *logrus.Logger {
	return h.logger
}

// Log returns the log entry object. Feel free to extend with more fields locally.
func (h *CLIRuntimeHelper) Log() *logrus.Entry {
	return h.log
}
