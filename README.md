[[_TOC_]]

# Terraform Module Registry for GitLab

This is a bare minimum implementation of the [Module Registry Protocol](https://www.terraform.io/docs/internals/module-registry-protocol.html) for GitLab, using a greedy init search and relying on instance-level [tag push events](https://gitlab.com/help/system_hooks/system_hooks#tag-events) for any subsequent updates. Works with group-level tag push events, too.

## Usage

Run in whatever way you prefer, as long as the application is exposed on a HTTPS endpoint that your `terraform` cli will be able to access and trust.

The application will listen on TCP port 3000. Visit `/modules/list` to get a list of modules.

## Starting the Application

Set `GITLAB_TOKEN` and optionally `CI_API_V4_URL` to control what GitLab server to connect to and what personal access token to use for that. However, _Terraform will only connect to HTTPS endpoints_, so deploy it behind a HTTPS endpoint.

Module downloads will need the user to first run `terraform login ${REGISTRY_FRIENDLY_HOSTNAME}`.

## Building in your fork

The project makes use of the Dependency Proxy feature in GitLab. To build in your personal namespace, you will need to add a CI/CD variable called `CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX` with the value `docker.io`. Make sure to uncheck the "Protected" checkbox when creating the variable, as it will otherwise not be available except on protected branches.

## TODO

  - [ ] Add a Redis cache or something to bettersupport multiple replicas
