package embeds

import (
	"embed"
)

// ViewFS contains all views and layouts.
var (
	//go:embed views/*
	ViewsFS embed.FS
	//go:embed assets/images/*
	Assets embed.FS
)
