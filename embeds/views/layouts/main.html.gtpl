{{ define "sign_in_out" -}}
  {{ $user := (index . "user") -}}
  {{ if not $user -}}
<form method="post" action="{{ reverse "sign_in" }}">
  <button tabindex="0" type="submit" title="Sign in with GitLab"><span>Sign in with</span><img src="/assets/images/gitlab-logo-1-color-black-rgb.svg" style="height: 2.2em"></span></button>
  <input type="hidden" name="csrf_token" value="{{ .token }}">
</form>
  {{ else }}
<span>Signed in as {{ $user.Name }} (<a href="{{ $user.WebURL }}">@{{ $user.Username }}</a>){{ if $user.Organization }} of {{ $user.Organization }}{{ end }} - </span>
<form method="get" action="{{ reverse "sign_out" }}">
  <button tabindex="0" type="submit" title="Sign out">Sign out</button>
  <input type="hidden" name="csrf_token" value="{{ .token }}">
</form>
  {{ end -}}
{{ end -}}
<!doctype html>
<html>
<head>
    <title>{{.title}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <style type="text/css">
    form {
        display: inline;
    }
    </style>
{{ template "head" . }}
</head>
<body>
    <main class="container">
{{ template "content" . }}
    </main>
    <footer class="footer">
      <div class="container">
        <span class="text-muted"><a href="{{ buildMeta "ciProjectURL" }}">Terraform Module Registry for GitLab</a> version {{ buildMeta "version" }}{{ with buildMeta "commit" }} ({{ . }}){{ end }}{{ with buildMeta "ciPipelineURL" }} built in <a href="{{ . }}">this GitLab Pipeline</a>{{ end }}.</span>
      </div>
    </footer>
</body>
</html>