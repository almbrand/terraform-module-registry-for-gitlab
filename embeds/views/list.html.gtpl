{{ define "head" -}}
<style type="text/css">
{{ chromacss "monokai" -}}
</style>
{{ end -}}
{{ define "content" -}}
<h1>Terraform Module Registry</h1>
  {{template "sign_in_out" .}}
<h2>Available modules</h2>
<ul>
    {{ range $index, $module := (index . "modules") -}}
    <li>
        <a href="{{ $module.URL }}">{{ $module.Namespace }}/{{ $module.Name }}/{{ $module.Provider }}</a>
        {{ if $module.LatestVersion -}}
        - <a href="{{ reverse "moduleUsage" $module.Namespace $module.Name $module.Provider $module.LatestVersion }}">usage</a>
        {{ else -}}
        - no tags yet
        {{ end -}}
    </li>
    {{ end -}}
</ul>
  {{ $showRefresh := (index . "showRefresh") -}}
  {{ $registryFriendlyHostname := index . "registryFriendlyHostname" -}}
  {{ if $showRefresh -}}
<form action="{{ reverse "moduleRefresh" }}" method="POST">
    <input type="hidden" name="csrf_token" value="{{ .token }}">
    <button type="submit">Refresh</button>
</form>
  {{ end -}}
  {{ if index . "user" -}}
<h2>Command-line login</h2>
<div>
    <p>Log in with</p>
    <div>
        <pre class="chroma" style="padding: 0.5em;">
            {{- chroma (printf "$ terraform login %s" $registryFriendlyHostname) "shell" "monokai" -}}
        </pre>
    </div>
    <p>to get access to non-public modules from your command-line.</p>
</div>
  {{ end -}}
<h2>Usage in pipelines</h2>
<div>
    {{ markdown "Put a `credentials.tfrc` file in a `.terraform.d` directory and  `terraform` should pick it up." }}
    <div>
        <pre class="chroma" style="padding: 0.5em;">
            {{- chroma (printf `  before_script:
  - |
    mkdir -p .terraform.d/
    cat <<EOF >.terraform.d/credentials.tfrc
    credentials %q {
      token = "${CI_JOB_TOKEN}"
    }
    EOF` $registryFriendlyHostname) "yaml" "monokai" -}}
        </pre>
    </div>
</div>
{{ end -}}