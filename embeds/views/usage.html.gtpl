{{ define "head" -}}
<style type="text/css">
{{ chromacss "monokai" -}}

th {
  background-color: #dbdbdba0;
  border-width: 1px;
  border-color: #bdbdbda0;
  border-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: bdbdbda0;
}

td {
  border-width: 1px;
  border-color: #bdbdbda0;
  border-style: solid;
  padding: .75rem;
}

.readme {
  border-width: 1px;
  border-color: #80808080;
  border-style: solid;
  padding: 1em;
  padding-left: 2em;
}
</style>
{{ end -}}
{{ define "content" -}}
{{ $module := index . "module" -}}
{{ $version := index . "version" -}}
{{ $latestVersion := index . "latestVersion" -}}
{{ $moduleVersion := index . "moduleVersion" -}}
{{ $moduleVersions := index . "moduleVersions" -}}
<h1>Terraform Module Registry</h1>
{{template "sign_in_out" .}}
<h2>Module <code>{{ $module }}</code> version <code>{{ $version }}</code></h2>
<ul>
  <li><a href="{{ reverse "moduleList" }}">← Back to module list</a></li>
  <li><a href="#usage">Usage</a></li>
  <li><a href="#readme">Readme</a></li>
  <li><a href="#cli-login">Command-line login</a></li>
  <li><a href="{{ $module.URL }}">Project page</a></li>
  <li><a href="{{ $module.URL }}/-/tree/{{ $moduleVersion.TagName }}">Browse version code</a></li>
</ul>
<form>
  <label for="version">Version</label>
  <select onchange="window.location.href = this.form.version.value;" name="version" id="version">
{{ range $index, $v := $moduleVersions -}}
    <option value="{{ $v.Version }}" {{ if eq $v.Version.String $version }} selected{{ end }}>{{ $v.Version }}{{ if $v.Version.Equal $latestVersion }} (latest){{ end }}</option>
{{ end -}}
  </select>
</form>
<h3 id="usage">Usage</h3>
<div>
  <pre class="chroma" style="padding:1em;">
    {{- chroma ($moduleVersion.ModuleUsageHCL registryFriendlyHostname) "terraform" "monokai" -}}
  </pre>
</div>
{{ if gt (len $moduleVersion.Readme) 0 -}}
<h3 id="readme"><a href="{{ $module.URL }}/-/blob/{{ $moduleVersion.TagName }}/README.md">Readme</a> from repository</h3>
<div class="readme">
  <blockquote>
    {{ markdown $moduleVersion.Readme }}
  </blockquote>
</div>
{{ end -}}
  {{ if index . "user" -}}
    {{ $registryFriendlyHostname := index . "registryFriendlyHostname" -}}
<h2>Command-line login</h2>
<div>
    <div>
        <pre class="chroma" style="padding: 0.5em;">
            {{- chroma (printf "$ terraform login %s" $registryFriendlyHostname) "shell" "monokai" -}}
        </pre>
    </div>
</div>
  {{ end -}}
{{ end -}}