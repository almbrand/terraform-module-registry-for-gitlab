package testhelpers

import (
	"net/http"
	"net/http/httptest"

	"github.com/gorilla/sessions"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/helpers"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/routes"
)

// SetupContext prepares a *httptest.Request and applies optional functions
// before creating a *httptest.ResponseRecorder with which to create a new
// echo.Context and ensures the router has mapped the given path in the
// echo.Context.
func SetupContext(e *echo.Echo, method string, target string, reqFuncs ...func(*http.Request, *httptest.ResponseRecorder)) (echo.Context, *httptest.ResponseRecorder) {
	req := httptest.NewRequest(method, target, nil)
	rec := httptest.NewRecorder()
	for _, reqFunc := range reqFuncs {
		reqFunc(req, rec)
	}
	c := e.NewContext(req, rec)
	e.Router().Find(method, target, c)
	return c, rec
}

// Middlewares applies session and auth middlewares to the given handler for
// tests which require those to be in place.
func Middlewares(appConfig *helpers.AppConfig, store sessions.Store, h func(c echo.Context) error) func(c echo.Context) error {
	for _, mw := range []echo.MiddlewareFunc{routes.AuthMiddleware(appConfig), session.Middleware(store)} {
		h = mw(h)
	}
	return h
}
