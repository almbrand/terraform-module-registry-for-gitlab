package testhelpers

import (
	"embed"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/helpers"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/repos"
)

// Fixtures provides a filesystem with a very minimal Terraform module.
var (
	//go:embed fixtures/*
	Fixtures embed.FS
	//go:embed invalid_module_fixtures/*
	InvalidModuleFixtures embed.FS
	//go:embed asset_fixtures/* asset_fixtures/subdir/*
	AssetFixtures embed.FS

	CIJobTokenUsername = "gitlab-ci-token"
	CIJobToken         = "CI_JOB_TOKEN_user1"
	DefaultUsers       = map[string]*gitlab.User{
		"user1": {
			ID:       1,
			Username: "user1",
			Email:    "user1@gitlab.example.com",
		},
		"user2": {
			ID:       2,
			Username: "user2",
			Email:    "user2@gitlab.example.com",
		},
		"user3": {
			ID:       3,
			Username: "user3",
			Email:    "user3@gitlab.example.com",
		},
		"user4": {
			ID:       4,
			Username: "user4",
			Email:    "user4@gitlab.example.com",
			IsAdmin:  true,
		},
		"user5": {
			ID:       5,
			Username: "user5",
			Email:    "user5@gitlab.example.com",
		},
		"user6": {
			ID:       6,
			Username: "user6",
			Email:    "user6@gitlab.example.com",
		},
	}
	DefaultMembers = map[string]gitlab.AccessLevelValue{
		"user1": gitlab.DeveloperPermissions,
		"user2": gitlab.MaintainerPermissions,
		"user3": gitlab.ReporterPermissions,
		"user5": gitlab.GuestPermissions,
	}
)

// SetupSingleRepo runs Setup with a single repository.
func SetupSingleRepo(t *testing.T, muxFuncs ...func(*http.ServeMux)) (*helpers.CLIRuntimeHelper, repos.ListerType, *echo.Echo, *httptest.Server, *gitlab.Client, error) {
	return Setup(t, MockCase{
		FixturesDir:  "fixtures",
		Users:        DefaultUsers,
		GroupMembers: DefaultMembers,
		ModuleRepos: []*MockModuleRepo{
			{
				ProjectID:         1,
				PathWithNamespace: "mock-modules/terraform-aws-public-module",
				Versions: NewMockVersions(t, []*MockModuleVersionInput{
					{
						TagName:     "v2.3.0",
						FS:          Fixtures,
						FixturesDir: "fixtures",
					},
					{
						TagName:     "v3.0.0-beta.1",
						FS:          Fixtures,
						FixturesDir: "fixtures",
					},
				}),
				ProjectMembers: DefaultMembers,
			},
		},
	}, muxFuncs...)
}

// SetupMultiRepo runs Setup with multiple repositories.
func SetupMultiRepo(t *testing.T, muxFuncs ...func(*http.ServeMux)) (*helpers.CLIRuntimeHelper, repos.ListerType, *echo.Echo, *httptest.Server, *gitlab.Client, error) {
	return Setup(t, MockCase{
		FixturesDir:  "fixtures",
		Users:        DefaultUsers,
		GroupMembers: DefaultMembers,
		ModuleRepos: []*MockModuleRepo{
			{
				ProjectID:         1,
				PathWithNamespace: "mock-modules/terraform-aws-public-module",
				Versions: NewMockVersions(t, []*MockModuleVersionInput{
					{TagName: "v1.2.3", FS: Fixtures, FixturesDir: "fixtures"},
					{TagName: "v2.3.0", FS: Fixtures, FixturesDir: "fixtures"},
					{TagName: "v2.1.4", FS: Fixtures, FixturesDir: "fixtures"},
					{TagName: "v1.2.7", FS: Fixtures, FixturesDir: "fixtures"},
				}),
			},
			{
				ProjectID:         2,
				PathWithNamespace: "mock-modules/terraform-azurerm-public-module",
				Versions: NewMockVersions(t, []*MockModuleVersionInput{
					{TagName: "v3.5.13", FS: Fixtures, FixturesDir: "fixtures"},
					{TagName: "v3.8.2", FS: Fixtures, FixturesDir: "fixtures"},
				}),
			},
			{
				ProjectID:         3,
				PathWithNamespace: "mock-modules/terraform-aws-private-module",
				Versions: NewMockVersions(t, []*MockModuleVersionInput{
					{TagName: "v2.3.3", FS: Fixtures, FixturesDir: "fixtures"},
					{TagName: "v5.6.18", FS: Fixtures, FixturesDir: "fixtures"},
					{TagName: "v3.1.9", FS: Fixtures, FixturesDir: "fixtures"},
				}),
				Visibility: gitlab.PrivateVisibility,
			},
		},
	}, muxFuncs...)
}
