package testhelpers

import (
	"context"
	"encoding/json"
	"fmt"
	"io/fs"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"testing"

	"github.com/hashicorp/go-version"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/embeds"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/helpers"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/repos"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/routes"
	"gitlab.com/almbrand/terraform-module-registry-for-gitlab/view"
	"golang.org/x/oauth2"
)

const (
	// ClientSecret is a constant client secret used in tests.
	ClientSecret = "CLIENT_SECRET"
)

// MockModuleRepo represents a mock ModuleRepo.
type MockModuleRepo struct {
	ProjectID         int
	PathWithNamespace string
	Versions          MockVersions
	Visibility        gitlab.VisibilityValue
	ProjectMembers    map[string]gitlab.AccessLevelValue
}

// MockVersions keeps MockModuleVersion by version and tag name.
type MockVersions struct {
	VersionsByVersion map[string]*MockModuleVersion
	VersionsByTagName map[string]*MockModuleVersion
}

// MockModuleVersion represents a mock ModuleVersion with files stored in a
// path -> contents map.
type MockModuleVersion struct {
	Version *version.Version
	TagName string
	Files   map[string]string // map from path to contents
}

// MockCase represents a test case used in the Setup method.
type MockCase struct {
	ModuleRepos  []*MockModuleRepo
	FixturesDir  string
	Users        map[string]*gitlab.User
	GroupMembers map[string]gitlab.AccessLevelValue
}

// MockModuleVersionInput is used in NewMockVersions to help representing a
// mock ModuleVersion with files coming from a fixtures directory.
type MockModuleVersionInput struct {
	TagName     string
	FS          fs.ReadDirFS
	FixturesDir string
}

// NewMockVersions builds MockVersions from a slice of MockModuleVersionInput,
// reading the contents from the provided filesystems into a Files field.
func NewMockVersions(t *testing.T, inputs []*MockModuleVersionInput) MockVersions {
	results := MockVersions{
		VersionsByVersion: map[string]*MockModuleVersion{},
		VersionsByTagName: map[string]*MockModuleVersion{},
	}
	for _, input := range inputs {
		version, err := version.NewSemver(input.TagName)
		if err != nil {
			continue
		}
		result := &MockModuleVersion{
			TagName: input.TagName,
			Version: version,
			Files:   map[string]string{},
		}

		fileInfos, err := input.FS.ReadDir(input.FixturesDir)
		require.NoError(t, err)
		for _, fileInfo := range fileInfos {
			if fileInfo.IsDir() {
				continue
			}
			f, err := input.FS.Open(filepath.Join(input.FixturesDir, fileInfo.Name()))
			require.NoError(t, err)
			contents, err := ioutil.ReadAll(f)
			require.NoError(t, err)
			result.Files[fileInfo.Name()] = string(contents)
		}

		results.VersionsByTagName[result.TagName] = result
		results.VersionsByVersion[result.Version.String()] = result
	}
	return results
}

func (mc MockCase) queryGroupMembers(query string) []*gitlab.GroupMember {
	result := []*gitlab.GroupMember{}
	for username, user := range mc.Users {
		switch query {
		case username, user.Email, strconv.Itoa(user.ID):
			level, exists := mc.GroupMembers[username]
			if exists {
				result = append(result, &gitlab.GroupMember{
					ID:          user.ID,
					Username:    username,
					AccessLevel: level,
				})
			}
		}
	}
	return result
}

// SetupServerClientMux creates a *httptest.Server and builds a GitLab client
// using its URL as a base URL, for mock purposes.
func SetupServerClientMux(t *testing.T) (*httptest.Server, *gitlab.Client, *http.ServeMux) {

	// mux is the HTTP request multiplexer used with the test server.
	mux := http.NewServeMux()

	// server is a test HTTP server used to provide mock API responses.
	server := httptest.NewServer(mux)

	// client is the Gitlab client being tested.
	client, err := gitlab.NewClient("", gitlab.WithBaseURL(server.URL))
	if err != nil {
		server.Close()
		t.Fatalf("Failed to create client: %v", err)
	}
	return server, client, mux
}

// Setup sets up a test HTTP server along with a gitlab.Client that is
// configured to talk to that test server.  Tests should register handlers on
// mux which provide mock responses for the API method being tested.
func Setup(t *testing.T, mockCase MockCase, muxFuncs ...func(*http.ServeMux)) (*helpers.CLIRuntimeHelper, repos.ListerType, *echo.Echo, *httptest.Server, *gitlab.Client, error) {
	server, client, mux := SetupServerClientMux(t)

	handleUserAuth(t, mockCase, mux)

	moduleRepos := map[string]*repos.ModuleRepo{}
	projects := []*gitlab.Project{}

	for _, repo := range mockCase.ModuleRepos {
		repo := repo
		projectPath := path.Base(repo.PathWithNamespace)
		namespaceFullPath := path.Dir(repo.PathWithNamespace)
		namespacePath := path.Base(namespaceFullPath)
		provider := repos.ModuleProjectName.FindStringSubmatch(projectPath)[1]
		visibility := repo.Visibility
		if visibility == "" {
			visibility = gitlab.PublicVisibility
		}
		project := &gitlab.Project{
			ID:                repo.ProjectID,
			Path:              projectPath,
			PathWithNamespace: repo.PathWithNamespace,
			Visibility:        visibility,
			Namespace: &gitlab.ProjectNamespace{
				ID:       1,
				Path:     namespacePath,
				FullPath: namespaceFullPath,
			},
		}
		projects = append(projects, project)
		moduleRepo := &repos.ModuleRepo{
			Project:   project,
			Namespace: namespacePath,
			Name:      strings.TrimPrefix(projectPath, fmt.Sprintf("terraform-%s-", provider)),
			Provider:  provider,
			URL:       "",
		}

		HandleProject(t, project, mux)
		handleRepoFiles(t, mockCase, mux, repo)
		handleRepoAccess(t, mockCase, mux, repo)
		handleRepoVersions(t, mux, repo, moduleRepo)

		moduleRepos[moduleRepo.Project.PathWithNamespace] = moduleRepo
	}
	HandleGroup(t, mockCase, mux, projects)

	// Build app configuration
	serverURL, err := url.Parse(server.URL)
	require.NoError(t, err)
	serverPort, err := strconv.Atoi(serverURL.Port())
	require.NoError(t, err)
	apiURLBase := *serverURL
	a := helpers.AppConfig{
		Debug: true,

		RegistryScheme:           serverURL.Scheme,
		RegistryPort:             serverPort,
		RegistryFriendlyHostname: serverURL.Hostname(),
		GitlabAPIV4URL:           &apiURLBase,
		ClientID:                 "APP_ID",
		ClientSecret:             ClientSecret,
		ValidXGitlabToken:        "VALID_X_GITLAB_TOKEN",
		ModulesGroupFullPath:     "mock-modules",
	}

	// Configure CLI runtime helper with logging and GitLab client fused to mux
	h := helpers.CLIRuntimeHelper{
		AppConfig: &a,
	}
	logger := logrus.StandardLogger()
	h.SetLogger(logger)
	log := logrus.NewEntry(logger)
	h.SetLog(log)
	h.SetClient(client)

	// Create module repo lister using GitLab client fused to mux
	lister := repos.NewLister(client, log, "mock-modules", false)

	// Configure views
	e := echo.New()
	e.Renderer = view.Engine(e, embeds.ViewsFS, "mock-modules", map[string]interface{}{})

	// Configure routes
	routes.ConfigureRoutes(&h, e, lister)

	return &h, lister, e, server, client, lister.Initialize(context.Background())
}

// HandleProject handles requests for the given project based on either its ID
// or its path with namespace.
func HandleProject(t *testing.T, project *gitlab.Project, mux *http.ServeMux) {
	projectFunc := func(rw http.ResponseWriter, r *http.Request) {
		require.NoError(t, json.NewEncoder(rw).Encode(project))
	}
	mux.HandleFunc(fmt.Sprintf("/api/v4/projects/%d", project.ID), projectFunc)
	mux.HandleFunc(fmt.Sprintf("/api/v4/projects/%s", project.PathWithNamespace), projectFunc)
}

func handleRepoFiles(t *testing.T, mockCase MockCase, mux *http.ServeMux, repo *MockModuleRepo) {
	func(projectID int, versionsByTagName map[string]*MockModuleVersion) {
		mux.HandleFunc(fmt.Sprintf("/api/v4/projects/%d/repository/tree", projectID), func(rw http.ResponseWriter, r *http.Request) {
			ref := r.URL.Query().Get("ref")
			moduleVersion, exists := versionsByTagName[ref]
			if !exists {
				rw.WriteHeader(http.StatusNotFound)
				return
			}
			rw.Header().Set("Content-Type", "application/json")
			rw.Header().Set("x-total-pages", "1")
			result := []*gitlab.TreeNode{}
			for path := range moduleVersion.Files {
				name := filepath.Base(path)
				result = append(result, &gitlab.TreeNode{
					Name: name,
					Path: path,
					Type: "blob",
				})
			}
			require.NoError(t, json.NewEncoder(rw).Encode(result))
		})
	}(repo.ProjectID, repo.Versions.VersionsByTagName)

	// Handle all file paths across refs
	paths := map[string]struct{}{}
	for _, moduleVersion := range repo.Versions.VersionsByTagName {
		for path := range moduleVersion.Files {
			paths[path] = struct{}{}
		}
	}
	for path := range paths {
		func(projectID int, path string, versionsByTagName map[string]*MockModuleVersion) {
			mux.HandleFunc(fmt.Sprintf("/api/v4/projects/%d/repository/files/%s/raw", projectID, path), func(rw http.ResponseWriter, r *http.Request) {
				ref := r.URL.Query().Get("ref")
				moduleVersion, exists := versionsByTagName[ref]
				if !exists {
					rw.WriteHeader(http.StatusNotFound)
					return
				}
				fmt.Fprintf(rw, moduleVersion.Files[path])
			})
		}(repo.ProjectID, path, repo.Versions.VersionsByTagName)
	}
}

func handleRepoAccess(t *testing.T, mockCase MockCase, mux *http.ServeMux, repo *MockModuleRepo) {
	// Combine access levels from group and project level
	combinedAccessLevels := map[string]gitlab.AccessLevelValue{}
	for username, level := range mockCase.GroupMembers {
		combinedAccessLevels[username] = level
	}
	for username, level := range repo.ProjectMembers {
		if existingLevel, exists := combinedAccessLevels[username]; exists && level < existingLevel {
			continue
		}
		combinedAccessLevels[username] = level
	}

	// Handle project membership requests
	for username, accessLevel := range combinedAccessLevels {
		func(username string, accessLevel gitlab.AccessLevelValue) {
			mux.HandleFunc(fmt.Sprintf("/api/v4/projects/%d/members/all/%d", repo.ProjectID, mockCase.Users[username].ID), handleMember(t, mockCase.Users, username, accessLevel))
		}(username, accessLevel)
	}
}

func handleMember(t *testing.T, users map[string]*gitlab.User, username string, accessLevel gitlab.AccessLevelValue) func(http.ResponseWriter, *http.Request) {
	return func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set("Content-Type", "application/json")
		require.NoError(t, json.NewEncoder(rw).Encode(&gitlab.ProjectMember{
			ID:          users[username].ID,
			Username:    username,
			Email:       users[username].Email,
			AccessLevel: accessLevel,
		}))
	}
}

func handleRepoVersions(t *testing.T, mux *http.ServeMux, repo *MockModuleRepo, moduleRepo *repos.ModuleRepo) {
	// Handle listing tags
	func(projectID int, versionsByTagName map[string]*MockModuleVersion) {
		mux.HandleFunc(fmt.Sprintf("/api/v4/projects/%d/repository/tags", projectID), func(rw http.ResponseWriter, r *http.Request) {
			result := []*gitlab.Tag{}
			for tag := range versionsByTagName {
				result = append(result, &gitlab.Tag{
					Name: tag,
				})
			}
			rw.Header().Set("Content-Type", "application/json")
			rw.Header().Set("x-total-pages", "1")
			require.NoError(t, json.NewEncoder(rw).Encode(result))
		})
	}(repo.ProjectID, repo.Versions.VersionsByTagName)

	// Create module versions from mock input
	moduleVersions := map[string]map[string]*repos.ModuleVersion{}
	versions := version.Collection{}
	moduleVersions[repo.PathWithNamespace] = map[string]*repos.ModuleVersion{}
	for version, mockVersion := range repo.Versions.VersionsByVersion {
		moduleVersion := &repos.ModuleVersion{
			ModuleRepo: moduleRepo,
			Version:    mockVersion.Version,
			TagName:    mockVersion.TagName,
		}
		moduleVersions[repo.PathWithNamespace][version] = moduleVersion
		versions = append(versions, mockVersion.Version)
	}
	if len(versions) > 0 {
		sort.Sort(sort.Reverse(versions))
		latestVersion := versions[0]
		if versions[0].Prerelease() != "" {
			for _, version := range versions {
				if version.Prerelease() == "" {
					latestVersion = version
					break
				}
			}
			moduleRepo.LatestVersion = latestVersion
		}
	}
}

// HandleGroup handles requests for the mock-modules group and its projects as
// well as group membership queries.
func HandleGroup(t *testing.T, mockCase MockCase, mux *http.ServeMux, projects []*gitlab.Project) {
	// Handle top level group requests
	mux.HandleFunc("/api/v4/groups/1", func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set("Content-Type", "application/json")
		rw.Header().Set("x-total-pages", "1")
		require.NoError(t, json.NewEncoder(rw).Encode(&gitlab.Group{
			ID:         1,
			Visibility: gitlab.PublicVisibility,
		}))
	})

	// Handle group membership requests
	for username, accessLevel := range mockCase.GroupMembers {
		func(username string, accessLevel gitlab.AccessLevelValue) {
			for _, groupID := range []interface{}{1, "mock-modules"} {
				mux.HandleFunc(fmt.Sprintf("/api/v4/groups/%v/members/%d", groupID, mockCase.Users[username].ID), handleMember(t, mockCase.Users, username, accessLevel))
			}
		}(username, accessLevel)
	}

	// Handle inherited group member requests
	mux.HandleFunc("/api/v4/groups/1/members/all", func(rw http.ResponseWriter, r *http.Request) {
		members := mockCase.queryGroupMembers(r.URL.Query().Get("query"))
		assert.NoError(t, json.NewEncoder(rw).Encode(members))
	})

	// Handle projects list requests
	mux.HandleFunc("/api/v4/groups/mock-modules/projects", func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set("Content-Type", "application/json")
		rw.Header().Set("x-total-pages", "1")
		require.NoError(t, json.NewEncoder(rw).Encode(projects))
	})
}

func handleUserAuth(t *testing.T, mockCase MockCase, mux *http.ServeMux) {
	// Handle current user requests
	mux.HandleFunc("/api/v4/user", func(rw http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("Authorization")
		if strings.HasPrefix(authorizationHeader, "Bearer ") {
			accessToken := strings.TrimPrefix(authorizationHeader, "Bearer ")
			user, exists := mockCase.Users[accessToken]
			require.True(t, exists)
			assert.NoError(t, json.NewEncoder(rw).Encode(user))
		} else {
			rw.WriteHeader(http.StatusNotFound)
		}
	})

	mux.HandleFunc("/oauth/token", func(rw http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodPost {
			require.NoError(t, r.ParseForm())
			switch r.FormValue("grant_type") {
			case "password":
				if r.FormValue("username") == CIJobTokenUsername && r.FormValue("password") == CIJobToken {
					rw.Header().Set("Content-Type", "application/json")
					assert.NoError(t, json.NewEncoder(rw).Encode(oauth2.Token{
						AccessToken: "user1",
						TokenType:   "Bearer",
					}))
					return
				}
			case "authorization_code":
				rw.Header().Set("Content-Type", "application/json")
				t.Logf("form values: %#v", r.PostForm)
				if strings.HasPrefix(r.FormValue("code"), "RETURNED_CODE_") {
					assert.NoError(t, json.NewEncoder(rw).Encode(oauth2.Token{
						AccessToken:  strings.TrimPrefix(r.FormValue("code"), "RETURNED_CODE_"),
						TokenType:    "Bearer",
						RefreshToken: "REFRESH_TOKEN",
					}))
					return
				}
			}
			rw.WriteHeader(http.StatusNotFound)
		}
	})
}

// Teardown closes the test HTTP server.
func Teardown(server *httptest.Server) {
	server.Close()
}
