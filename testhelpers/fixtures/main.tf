terraform {
  required_providers {
    random = {
      source  = "hashicorp/random"
      version = "~> 3"
    }
  }
  required_version = ">= 0.14"
}

variable "object" {
  description = "desc"
  type = object({
    s = string
    b = bool
  })
  default = {
    s = "foo"
    b = false
  }
}

variable "object_no_default" {
  type = object({
    s = string
    b = bool
  })
}

variable "string_no_description" {
  type = string
}

variable "string_no_description_sensitive" {
  type      = string
  sensitive = true
}

output "literal" {
  description = "output description"
  value       = "baz"
}

output "literal_no_description" {
  value = "baz"
}

output "sensitive" {
  value     = var.string_no_description_sensitive
  sensitive = true
}
